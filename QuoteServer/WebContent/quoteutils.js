function toggleVis(elementId) {
	if (document.getElementById) {
		var s = document.getElementById(elementId).style;
		if (s.display == "block") {
			s.display = "none"; 
		} else {
			s.display = "block"; 
		}
		return false; 
	} else {
		return true; 
	}
}
