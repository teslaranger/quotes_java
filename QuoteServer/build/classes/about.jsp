<%@ page language="java"
		 contentType="text/html; charset=ISO-8859-1"
		 pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>About &ldquo;Quooqle&rdquo;</title>
	</head>
	<body>
		<h1>Some Questions and Answers About Quooqle</h1>
		<p>QUESTION: How do you pronounce &quot;Quooqle&quot;?<br>ANSWER:  Say &quot;kwoo kull&quot;.</p>
		<p>QUESTION: What is Quooqle?<br>ANSWER:  Quooqle is a search-engine for quotations.  Got a favorite saying but don't know who said it.  Quooqle it!</p>
		<p>QUESTION: Does Quooqle have a desktop app for Mac OS X?<br>ANSWER: Coming soon!</p>
		<p>QUESTION: Does Quooqle have a desktop app for Windows?<br>ANSWER: A Java app is coming soon.</p>
		<p>QUESTION: Does Quooqle have a desktop app for Linux?<br>ANSWER: A Java app is coming soon.</p>
		<p>QUESTION: Does Quooqle  have a mobile app?<br>ANSWER: A iOS (iPhone/iPad/iPod Touch, etc) app is coming soon.  An Android app is being considered since Quooqle uses Java where possible and Android uses a clone of Java.</p>
		<p>QUESTION:  Why do I keep typing &quot;Quoogle&quot; instead of &quot;Quooqle&quot;?<br>ANSWER: Those darn lowercase Q's and G's look a lot alike!</p>
		
		<p>QUESTION: How do I contact Quooqle for help.<br>
		ANSWER: Since Quooqle is presently a free service, free support is not also available.  
		You can email us at help@quooqle.com and we'll answer as we have time and resources available. No promises.  
		Remember: <a href="http://localhost:8080/QuoteServer/QuoteSearch?searchstring=tanstaafl&quotepersonboth=quote&searchmethod=wholephrase&graybar=true&casesensitive=false">TAANSTAFL</a>: (There Ain't No Such Thing As A Free Lunch.).
		</p>
		<div class="alignleft">
		     <script type="text/javascript">
		       	amzn_assoc_ad_type = "banner";
				amzn_assoc_marketplace = "amazon";
				amzn_assoc_region = "US";
				amzn_assoc_placement = "assoc_banner_placement_default";
				amzn_assoc_campaigns = "officeschoolsupplies";
				amzn_assoc_banner_type = "category";
				amzn_assoc_p = "12";
				amzn_assoc_isresponsive = "false";
				amzn_assoc_banner_id = "118T0Y7A66XWR2080AG2";
				amzn_assoc_width = "300";
				amzn_assoc_height = "250";
				amzn_assoc_tracking_id = "williambdavis-20";
				amzn_assoc_linkid = "9cfd97e97ef7230fcd9fb11953feb139";
		     </script>
		     <script src="//z-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&Operation=GetScript&ID=OneJS&WS=1"></script>
		</div>
	</body>
</html>
