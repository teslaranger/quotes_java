<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="com.quooqle.quoteserver.QuoteCache" %>
<%@ page import="com.quooqle.quoteserver.HtmlUtils" %>
<html>
	<head>
		<title>&ldquo;Quooqle&rdquo;</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" href="index.css" />
		<link rel="stylesheet" href="RandomQuote.css" />
		<link rel="stylesheet" href="randomcolors1.css" />
		<script src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script type="text/javascript">
			// FUTURE: search for quotes by people born today or said on this date of the year.
			// NOTE: Default quote search string should be ?searchstring=quot this will result in quotations about quotations being displayed!
			
				var p_attributes = "onmouseover=\"changeElementColor(this, 'lavender', 'gray');\" onmouseout=\"changeElementColor(this,'white', 'black');\" style=' width:100%; text-align: center; '";
				function changeQuote()
				{
					// call GetQuoteAjax servlet to get a random quote
				    $.get('GetRandomQuote?searchstring=quot', function(quote) { document.getElementById('therandomquote').innerHTML="<p " + p_attributes + ">"+quote+"</p>"; } );
					var t = setTimeout('changeQuote()',60000);
					// use JSP call here to QuoteCache.getInstance().getRandomQuotation() to fill a JavaScript array
					// or find a to call Java servlent from JavaScript?
				}
				
				function getQuote() {
					// call GetQuoteAjax servlet to get a random quote
				    $.get('GetRandomQuote?searchstring=quot', 
						    function(quote) { document.getElementById('therandomquote').innerHTML="<p " + p_attributes + ">"+quote+"</p>"; } );
				}

				function getDatedQuote(date) {
					// date=yyyymmdd
					alert("in getDatedQuote("+date+")");
					$.get('GetRandomQuote?date='+date, 
						    function(quote) { document.getElementById('thedatequote').innerHTML="<p " + p_attributes + ">"+quote+"</p>"; } );
				}
				
				// clock display - click to get a dated quote if any are available.
				
				var clockAttributesHead = "<p id='datetime' onclick=\"getDateQuote('";
				var clockAttributesMiddle = "');\" >";
				var clockAttributesFooter = "</p>";

				function getDate() {
					var today = new Date();
					var mon   = today.getMonth()+1;
					var day   = today.getDate();
					var year  = today.getFullYear();
					// add a zero in front of numbers<10
					mon = leadingZeroes(mon);
					day = leadingZeroes(day);
					var thedate = year+mon+day
					//alert("The date:"+thedate);
					return year+mon+day;  //yyyymmdd
				}
						
				function startTime()
				{
					var today=new Date();
					var weekday=today.getDay();
					var mon=today.getMonth()+1;
					var day=today.getDate();
					var year=today.getFullYear();
					var h=today.getHours();
					var m=today.getMinutes();
					var s=today.getSeconds();
					// add a zero in front of numbers<10
					m=leadingZeroes(m);
					s=leadingZeroes(s);
					document.getElementById('clock').innerHTML= mon+"/"+day+"/"+year+" "+h+":"+m+":"+s;
//					document.getElementById('clock').innerHTML	= clockAttributesHead 
//															 	+ mon+"/"+day+"/"+year 
//																+ clockAttributesMiddle 
//																+ mon+"/"+day+"/"+year+" "+h+":"+m+":"+s
//																+ clockAttributesFooter;	
					st=setTimeout('startTime()',500);
				}

				function leadingZeroes(i)
				{
					if (i<10)
					  {
					  i="0" + i;
					  }
					return i;
				}

				var oldcolor;

				function getStyle(el,styleProp)
				{
					var x = document.getElementById(el);
					if (x.currentStyle)
						var y = x.currentStyle[styleProp];
					else if (window.getComputedStyle)
						var y = document.defaultView.getComputedStyle(x,null).getPropertyValue(styleProp);
					return y;
				}

				function changeColor2(id, color) {
					element = document.getElementById(id);
					event.cancelBubble = true;
					if (color=0) {
						element.style.background = oldcolor;
					} else {
						oldColor = getStyle(element, 'background')//element.currentStyle.background;
						element.style.background = color;
					}
				}	

				function changeElementColor(element, bgcolor, fgcolor) {
					element.style.background=bgcolor;
					element.style.color=fgcolor;
				}

				function changeColor(id, bgcolor, fgcolor){
					document.getElementById(id).style.background=bgcolor;
					document.getElementById(id).style.color=fgcolor;
				}
							
			</script>
	</head>
	<body onload='changeQuote();  startTime(); '>
		<p>&nbsp;</p>
		<div class='header' id='header' style='text-align: center; height: 4em; '>	
			<%= HtmlUtils.getLogoHtml() %>
		</div>
		<div>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
		</div>
		<div id='therandomquote' style=" text-align: center; height: 3em; "
			 onclick="location='/QuoteServer/QuoteSearch?searchstring=quot'" >
			<p id='quotetext' style=' text-align: center; color: black; background-color: white; '><i>Searching...</i></p>
		</div>
		<p>&nbsp;</p>
		<div id='search' style='text-align: center; '>
			<table cellpadding=0 cellspacing=0 border=0 width=100%>
				<tr valign=top >
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
					<td>&nbsp;&nbsp;</td>
					<td align=center nowrap><%= HtmlUtils.displayQuoteSearchForm() %></td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
		<p>&nbsp;</p>
		<div id="linkbar" id="linkbar" style=' text-align: center; '>
			<a href="about.jsp">About</a> | 
			<a href="news.jsp">News</a> | 
			<a href="/QuoteServer/RandomQuote">Random Quote</a> | 
			<a href="/QuoteServer/RandomFortune">Random Fortune</a>
		</div>
		<p>&nbsp;</p>
<%-- 
		<div id="clock" style='text-align: center;' onmouseover="changeElementColor(this, 'lavender', 'gray');" onmouseout="changeElementColor(this,'white', 'black');">00:00:00</div>
		<div id="thedatequote" style='text-align: center; color: gray; font-style: italic;' onclick='getDatedQuote(getDate());' ><p>(Click the date and time for a quotation by someone born today or that was said on this day of the year, if one is available!)</p></div>		
--%>
		<div id="center_ad" style="text-align: center; ">
			<div id="amazon_ad" style="display: inline-block; ">
			     <script type="text/javascript">
					amzn_assoc_ad_type = "banner";
					amzn_assoc_marketplace = "amazon";
					amzn_assoc_region = "US";
					amzn_assoc_placement = "assoc_banner_placement_default";
					amzn_assoc_campaigns = "officeschoolsupplies";
					amzn_assoc_banner_type = "category";
					amzn_assoc_p = "12";
					amzn_assoc_isresponsive = "false";
					amzn_assoc_banner_id = "118T0Y7A66XWR2080AG2";
					amzn_assoc_width = "300";
					amzn_assoc_height = "250";
					amzn_assoc_tracking_id = "williambdavis-20";
					amzn_assoc_linkid = "9cfd97e97ef7230fcd9fb11953feb139";
			     </script>
			     <script src="//z-na.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&Operation=GetScript&ID=OneJS&WS=1"></script>
			</div>
		</div>
		<div class='footer'>	
			<p id='footertext' style='position:fixed; bottom: 0em; left: 0em; color: black; background-color: lightgrey; width: 100%'>
				Copyright &copy; 2011-<script>document.write(new Date().getFullYear())</script> William B. Davis, Jr. All Rights Reserved.  
			</p>
		</div>
	</body>
</html>
