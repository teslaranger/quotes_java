<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
		<div id='body'>
			<hr style='color: green; '> 
			<div class="news">
				<h5>NEWS!</h5>
				<ul>
					<li>You can now search for other quotations by a person by clicking on their name in the search results!</li>			
				</ul>
				<hr style='color: green; '> 
				<h5>FUTURE PLANS</h5>
				<ul>
					<li>Editing and saving of in memory database</li>
					<li>Removal of selected duplicate quotations</li>
					<li>Automatic removal of exact-match duplicates</li>
					<li>Addition of new quotations from a form, perhaps the clipboard (JavaScript to fill the form or recognize one on the clipboard)</li>
					<li>Selection of different quote databases</li>
					<li>Exporting and importing of quote databases to/from the quote server</li>
					<li>Search on any selected/double-clicked word or phrase on the screen? Use a shortcut key with a button for that?</li>
				</ul>
				<hr style='color: green; '> 
			</div>
		</div>			

</body>
</html>