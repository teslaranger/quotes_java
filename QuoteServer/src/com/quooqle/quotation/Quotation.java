package com.quooqle.quotation;

import java.io.Serializable;

public class Quotation implements Serializable, QuotationInterface {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 5457932777518210054L;
	//TODO: Make these private for encapsulation, use getter/setters?
	public long 	id;			// quotation id
	public String 	text;		// quotation text
	public String 	person; 	// who said it
	public String	vocation;	// vocations(s) of person (make string array?)
	public String	context;	// book quote is from, location where speech given, etc.
	public String	yearBorn;	// date person who said quote was born, null if unknown
	public String	yearDied;	// date person who said quote died, null if not dead.
	public int		starRating;	// star rating, 1-5, default 0 (unrated)
	public String	dateOfQuote;// or just year. mm/dd/yyyy or just yyyy.
	public String	attribution;// the original attribution text before splitting out the above fields, if available.
	//public String[] categories;	//TODO: Add categories this quote fits in; store in text file as ['cat1, cat2, cat3, cat4']  Or use a List<String>/ArrayList<String>


	//Constructors
	public Quotation() {
		//nullary (no-argument) constructor for serialization
	}

	public Quotation(	long 	newId,
						String	newText,
						String 	newPerson,
						String	newVocation,
						String 	newContext,
			 			String	newYearBorn,
			 			String 	newYearDied,
			 			int		newStarRating,
			 			String  newDateOfQuote,
			 			String  newAttribution
					)
	{

		id         	= newId;
		text       	= newText;
		person 	 	= newPerson;
		vocation   	= newVocation;
		context    	= newContext;
		starRating	= newStarRating;
		yearBorn   	= newYearBorn;
		yearDied   	= newYearDied;
		dateOfQuote = newDateOfQuote;
		attribution = newAttribution;
	}

	//TODO: Create a constructor that takes two lines: quote and attribution, and splits out all the fields.  Use code from the handleTxtFile method in Quotes.java
	
	public String toString() {

		String quote = "";
		
		quote = "\"" + this.text + "\"" + "\n"
			  + "  - " 
			  + ((this.person.equals(""))           ? "Unknown" : this.person)
		      + ((this.vocation.equals(""))         ? ""        : ", " + this.vocation)
		      + ((this.context.equals(""))          ? ""        : ", '" + this.context + "'");		

		if (this.yearBorn.equals("")) 
		{
	        if (this.yearDied.equals("")) {
	        	// do nothing; both born and died are empty, don't display anything
	        } else 
	        	// we have a year died but not a year born
	        	quote = quote + " (?-" + this.yearDied + " )";
			}
		else 
		{
			if (this.yearDied.equals("")) 
			{
				// year born present, but no year died; probably still living
				quote = quote + " (" + this.yearBorn + "- )";
			} 
			else 
			{
				// both year born and year died present
				quote = quote + " (" + this.yearBorn + "-" + this.yearDied + ")";
			}
		} 
		
		quote = quote + ((this.starRating == 0)            ? ""        : " " + starsString(this.starRating))
		              + ((this.dateOfQuote.equals(""))     ? ""        : " (" + this.dateOfQuote + ")");

		return quote;
	}


	public String starsString() {

			return starsString( this.starRating );
	}

	//TODO: Implement half stars? 
	public String starsString( int starRating ) {

			//TODO: find a java standard method to make a string of 'n' specific chars, given 'n' and the char.

			String stars = "";

			for (int i=0; i < starRating; i++) {
				
				stars = stars + "*";		
				
				// black star = U+2605 decimal 9733;  white star = U+2606 decimal 9734 

				//stars = stars + "\u2605";								
			}

			// fill in rest of 5 chars with "."
			while (stars.length() < 5) 
			{
				//stars = stars + "\u2606";
				stars = stars + ".";
			}
			
			return stars;
	}

	// Getters and setters

	public long getId () {
		return this.id;
	}

	public void setId (long id) {
		this.id = id;
	}

	public String getText () {
		return this.text;
	}

	public void setText (String text){
		this.text = text;
	}

	public String getPerson() {
		return this.person;
	}
	
	public void setPerson (String person) {
		this.person = person;
	}
	
	public String getVocation () {
		return this.vocation;
	}

	public void setVocation (String vocation) {
		this.context = vocation;
	}

	public String getContext () {
		return this.context;
	}

	public void setContext (String context) {
		this.context = context;
	}

	public String getYearBorn () {
		return this.yearBorn;
	}

	public void setYearBorn (String year) {
		this.yearBorn = year;
	}

	public String getYearDied () {
		return this.yearDied;
	}

	public void setYearDied (String year) {
		this.yearDied = year;
	}

	public int getStarRating () {
		return this.starRating;
	}

	public void setStarRating (int stars ) {
		this.starRating = stars;
	}

	public String getDateOfQuote() {
		return this.dateOfQuote;
	}
	
	public void setDateOfQuote(String date) {
		this.dateOfQuote = date;
	}
	
	public String getAttribution () {
		return this.person;
	}

	public void setAttribution (String person) {
		this.person = person;
	}

}
