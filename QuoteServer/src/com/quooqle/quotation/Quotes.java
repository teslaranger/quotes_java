package com.quooqle.quotation;


import java.io.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.quooqle.util.String.CSVParser;
import com.quooqle.util.String.Trim;

public class Quotes {

	private static boolean inDebugMode = false;
	
	
	public static void main(String... args) {
		
		//HANDLE COMMAND LINE ARGUMENTS
		
		// Command line params are:
		//		 0: command(s) to perform. May be multiple commands, separated by commands (NO spaces!)
		//				random  	- display a random quote from the specified file (default)
		//				dump		- display all the quotes from the specific filed in readable format
		//				list		- same as dump
		//				debug		- turn on debug mode
		//				csvdump 	- output the quotes read from the input file in CSV format
		//				csvexport	- same as csvdump
		//
		//       TODO: Implement more commands:
		//				find"string"   	- no space after find
		//				search"string" 	- no space after find
		//				grep"regex"    	- no space after "grep", regex is a regular expression. 
		//				print			- print a random quote to printer 
		//
		//			    Implement saving/restoring of quotes via Java serialization.  Need to be able to read in CSV etc, write serialized 
		//					objects, and vice versa.  Serialized objects will be more reliable for READING once written. 
		//
		//
		//       1: filespec of quote file to read.  CSV and VB6 Quotes.exe app format supported, FUTURE: XML and flat file from Flat2XML project
		//
		//TODO: May want to reverse this to allow for 
		//     java Quotes Quotes.txt csvexport export.csv
		//
		//TODO: Create "CommandLine" collection and "Command" objects? Investigate Collections versus Lists.

		// Default command and filespec parameters
		String command  = "random";			//args[0]
		String filespec = "Quotes.txt";		//args[1]
						
		if (args.length > 0) {
			command = args[0].toLowerCase().trim();
		}

		// split apart separate comma-separated commands in args[0] and put into a List so we can use list.contains("string") to see if command is present.
		String[] commandArray = command.split(",");
		List <String> commands = Arrays.asList(commandArray);

		if (commands.contains("debug")) {
			inDebugMode = true;
		}

		if (inDebugMode) {
			for (String cmd: commands) {
				System.err.println(cmd);
			}
		}
			
		if (args.length > 1) {
			filespec = args[1];
			filespec = Trim.leadingAndTrailing(filespec,"\"");  // discard any quotes from around filespec on command line
		} 

		System.err.println(new File(filespec).getAbsoluteFile());

		
		List<Quotation> quotes;	 //TODO: Create a Quotes object that's a list/collection of Quotation objects?		
		quotes = readQuotesFromFile(filespec);
	
		
		// PROCESS COMMANDS		
		
		if ( (commands.contains("dump")) || (commands.contains("list")) ) {
			simpleQuoteList(quotes);
		}
		
		if ( (commands.contains("csvdump")) || (commands.contains("csvexport")) ) {
			exportQuotesAsCsv(quotes);
		}

		if ( (commands.contains("random")) || (commands.size() == 0) ) {
			System.out.println(getRandomQuote(quotes));
		}		

		if (commands.contains("repeat")) {
			outputRandomQuoteAndRepeat(quotes);
		}

	}

	public static void outputRandomQuoteAndRepeat(List <Quotation> quotes) {
		do {	
			System.out.println(getRandomQuote(quotes));			
			try {
				Thread.sleep(5000);	// in milliseconds; 1000 milliseconds  = 1 second
			} catch (Exception e) {
				// do nothing
			}
		} while (true);		
	}
	
	public static void simpleQuoteList(List<Quotation> quotes) {
		// Quote and person ONLY.
		for (int i=0; i < quotes.size(); i++) 
		{
			System.out.println("");
			System.out.println(quotes.get(i).text);
			System.out.println(" - " + quotes.get(i).person);
			System.out.println("");
		}
	}
	
	public static String getRandomQuote(List<Quotation> quotes) {
		//int r = (int)(Math.random()*quotes.size());
		Random rnd = new Random();
		int r = rnd.nextInt(quotes.size());
		
		return quoteToString(quotes,r);
	}
	
	public static String quoteToString(List<Quotation> quotes, int quoteNumber) {
		String quoteAsString = "\n" 
							 + "Quote #"+(quoteNumber+1)+" of " + quotes.size() + ":"
							 + "\n"
							 +  quotes.get(quoteNumber).toString()
							 + "\n";
		return quoteAsString;
	}

	public static void exportQuotesAsCsv(List<Quotation> quotes) {
		exportQuotesAsCsv(quotes, true, "");
	}

	public static void exportQuotesAsCsv(List<Quotation> quotes, boolean withHeadings, String filespec) 
	{
		FileWriter file = null;
		BufferedWriter buffer = null;
		
		if (!(filespec.equals(""))) {
			File csvFile = new File(filespec);
			
			if (csvFile.exists()) {
				// file exists; do we overwrite or not?
				System.err.println("CSV File " + filespec + " already exists; not written.");
				return;
			} else {
				// file does not exists, create new.
				try {
					file = new FileWriter(filespec);
					buffer = new BufferedWriter(file);
				} 
				catch (IOException e) {
					System.err.println("Error opening CSV export file " + filespec);
					System.err.println("  Error message: " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
		
		// Output optional CSV Headings
		//TODO: Move to separate method()

		if (withHeadings) {
			//TODO: Put these fields in quotes too?
			//System.out.println("ID,Quote,Person,Vocation,Context,YearBorn,YearDied,Stars");

			//Pull out the field names from a class and use them as the column headings.
			Field[] fields = Quotation.class.getFields();
			
			String heading = null;
			
			for (Field f: fields) {
				heading = heading +  f.getName() + ",";
			}

			if (filespec.equals("")) {
				System.out.println(heading);
			} else {
				try {
					buffer.write(heading);
				} catch (IOException e) {
					System.err.println("Error writing heading to CSV file " + filespec);
					System.err.println("  Error message: " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
		
		// Then output the fields, strings in quotes, fields separated by commas.
		//TODO: Output all these fields using the fields[] array from above somehow; any guarantee these will be in order declared in the class?
		for (int i=0; i < quotes.size(); i++) 
		{	
			Quotation q = quotes.get(i);	
			String csvLine = outputCsvLine(q);
			
			if (filespec.equals("")) {
				//output CSV lines to standard out channel, use i/o redirection to output to a file if you want (e.g. > filespec.csv on end of command line)
				System.out.println(csvLine);
			} else {
				try {
					buffer.write(csvLine);
				} catch (IOException e) {
					System.err.println("Error writing line to CSV file " + filespec);
					System.err.println("  Error message: " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
		
		try {
			buffer.close();
		} catch (IOException e) {
			System.err.println("Error closing buffer after writing CSV file " + filespec);
			System.err.println("  Error message: " + e.getMessage());
			e.printStackTrace();
		}
		//file.close(); is not necessary?  Seems to do same thing as buffer.close, based on documentation (call .close in Writer);		
	}

	//TODO: Move to Quotation.java
	public static String outputCsvLine(Quotation q) {

		final String sepChar = ",";
		final String quoteChar = "\"";
		
		String csvLine = q.id  
					   + sepChar 
					   + quoteChar
					   + q.text
					   + quoteChar
					   + sepChar
					   + quoteChar
					   + q.person
					   + quoteChar
					   + sepChar
					   + quoteChar
					   + q.vocation
					   + quoteChar
					   + sepChar
					   + quoteChar
					   + q.context
					   + quoteChar
					   + sepChar
					   + quoteChar
					   + q.yearBorn
					   + quoteChar
					   + sepChar
					   + quoteChar
					   + q.yearDied
					   + quoteChar
					   + sepChar
					   + q.starRating   //or: q.starsString() or q.starsString(q.starRating)
					   + sepChar
					   + quoteChar
					   + q.dateOfQuote
					   + quoteChar
					   + sepChar
					   + quoteChar
					   + q.attribution
					   + quoteChar;				
				   	   		
		return csvLine;
	}
	
	
	//TODO: Move to QuoteFileReader.java
	
	public static List<Quotation> readQuotesFromFile(String filespec) {

		List <Quotation> quotes = new ArrayList<Quotation>();
		
		File quoteFile = new File(filespec);
		
		if (!quoteFile.exists()) {
			System.err.println("File " + filespec + " does not exist.");
			return null;
		}
		
		if (filespec.toLowerCase().endsWith(".txt")) 
		{
			quotes = handleTxtFile(quoteFile);
		} 
		else if (filespec.toLowerCase().endsWith(".csv")) 
		{					
			quotes = handleCsvFile(quoteFile);
		}  
		else if (filespec.toLowerCase().endsWith(".xml"))
		{			
			quotes = handleXmlFile(quoteFile);
		}
		else if (filespec.toLowerCase().endsWith(".flat")) 
		{
			quotes = handleFlatFile(quoteFile);
		} 
		else 
		{
			// unknown file type
			System.err.println(quoteFile.getName() + " is an unknown file type; please try a different file.");
			//TODO: throw file not found exception or something?
		}			
		
		return quotes;
	}
	
	//TODO: Create an QuoteFileReader class (interface? abstract class?) and make all the handle*File() methods be subclasses. 
	
	public static List<Quotation> handleTxtFile(File quoteFile) {

		String quote;
		String attribution;
		String vocation;
		String context;
		String yearBorn;
		String yearDied;
		int starRating; //default = 0 (unrated)
		String stars;
		String dateOfQuote;
		String originalAttribution; 
		
		long lineCount  = 0L;
		long quoteCount = 0L;

		List <Quotation> quotes = new ArrayList<Quotation>();
		
		try 
		{
			FileReader quoteReader = new FileReader(quoteFile);
			BufferedReader buffer = new BufferedReader(quoteReader);

			while ((quote = buffer.readLine()) != null)
			{
				lineCount++;

				attribution = "";
				vocation = "";
				context = "";
				yearBorn = "";
				yearDied = "";
				stars = "";
				starRating = 0; //default = 0 (unrated)
				dateOfQuote = "";
				originalAttribution = "";

				if (quote.trim().equals("")) 
				{
					// ignore blank lines
					continue;
					
				} else 
				{
					// we have quote text text;
					if (quote.trim().startsWith("-")) 
					{
						// it is an attribution line; if so, it's out of order.
						System.err.println("/!\\ Warning: Skipping an attribution line out of order at quote # " + quoteCount + " line #" + lineCount +": "+ quote);
						continue;
					}

					// clean up contents of quote to avoid problems with embedded quotation marks already in the quote.
					
					quote = Trim.leadingAndTrailing(quote, "\"");			// strip leading/trailing quotes
					quote = quote.replace("\"", "'");		// transform internal double-quotes to single-quotes. Excel hates internal quotes and escapes. Handles very strangely.						
					
					// get next line, assume it's the attribution (who/when quote was said/written)
					attribution = buffer.readLine().trim(); 
					lineCount++;
					
					if (attribution.equals("")) 
					{
						System.err.println("/!\\ Warning: Line #" + lineCount + ": Found a blank line after the quote line, so assume no attribution for this quote:");
						System.err.println(quote);
						attribution = "Unknown";	
					} 
					else 
					{
//						We got a non-blank line, assume it is attribution
						
						originalAttribution = attribution;					// keep it pre-modification and extraction
																			//TODO: keep original quote text too, unmodified?
																			//TODO: Make a ConvertedQuote object extending Quote, with those fields, for use in file conversion.

						//TODO: Perhaps we should NOT do some of the following as it may screw up following extractions of years, dates, etc.
						//TODO: Change trimChar to use "^[\\"-]" and "[\\"]$" to remove leading/trailing quotes?
						attribution = Trim.leadingAndTrailing(attribution,"\"");			// remove leading/trailing quotes
						attribution = Trim.leadingAndTrailing(attribution,"�");			// remove curly open-quotes.
						attribution = Trim.leadingAndTrailing(attribution,"�");			// remove curly close-quotes.
						//TODO: How about curly single quotes:  � �
						attribution = attribution.trim();  					// remove any leading whitespace AGAIN after removing surrounding quotes.
						attribution = Trim.leadingAndTrailing(attribution,"-");			// chop off any leading "-" char(s)
						attribution = Trim.leadingAndTrailing(attribution,"�");			// also em-dash (or is it an en-dash?)
						//TODO: add removing of other typographic dash (em or en) in leading position.
						//TODO: need trimLeadingChar() and trimTrailingChar() methods, have trimChar() use both, rename it to trimLeadingAndTrailingChars();
						attribution = attribution.trim(); 					// remove any leading whitespace AGAIN after removing the starting "-".
						attribution = attribution.replace("\"","'");		// transform internal double-quotes to single-quotes. Excel hates internal quotes and escapes. Handles very strangely.

						if (inDebugMode) System.err.println(originalAttribution);
						if (inDebugMode) System.err.println(attribution);
						
//						Objects for regex string searches below.
						Pattern pattern;
						Matcher matcher;
						
						//TODO: Check for "-" or en-dash or em-dash or non-alphanumeric at start that isn't a quote?

						// Find born/died years of the form "(yyyy-yyyy)" (with possible spaces
						String regexYearBornDied = "[, (/]{1}[0-9]{4}[ ]*-[ ]*([0-9]{4})*[), ]{1}";	
						String yearBornDied = "";
						pattern = Pattern.compile(regexYearBornDied);
						matcher = pattern.matcher(attribution); 
						if (matcher.find()) {										
							yearBornDied = matcher.group();
							if (inDebugMode) System.err.println(yearBornDied);
						}						
												
//						 Find year of form "(yyyy-"  from "(yyyy-yyyy)" found above and extract as year born
						String regexYear = "[0-9]{4}";	
						pattern = Pattern.compile(regexYear);
						matcher = pattern.matcher(yearBornDied); 
						if (matcher.find()) {			
							yearBorn = matcher.group();
							if (inDebugMode) System.err.println("Born:"+yearBorn);
						}						
						if (matcher.find()) {
							yearDied = matcher.group();
							if (inDebugMode) System.err.println("Died:"+yearDied);
						}						

						// remove (yyyy-yyyy) from attribution string
						if (!yearBorn.equals("")) {
							if (inDebugMode) System.err.println(attribution);	
							attribution = attribution.replaceFirst(regexYearBornDied,"");
							if (inDebugMode) System.err.println(attribution);	
						}
						
//						Extract stand-alone year of quote of form "9999" surrounded only by parens, spaces or commas (or EOL?) as year of quote using regex
						String regexYearOfQuote = "[, (]{1,}[0-9]{4}[), ]{1,}";
						pattern = Pattern.compile(regexYearOfQuote);
						matcher = pattern.matcher(attribution);
						if (matcher.find()){
							if (inDebugMode) System.err.println("Year of quote found:"+matcher.group());
							Pattern pattern2=Pattern.compile(regexYear);
							Matcher matcher2 = pattern2.matcher(matcher.group());
							if (matcher2.find()) {
								dateOfQuote = matcher2.group();
								if (inDebugMode) System.err.println("Year of quote extracted:"+ dateOfQuote);
								if (inDebugMode) System.err.println(attribution);
								attribution = attribution.replaceFirst(regexYearOfQuote,"");
								if (inDebugMode) System.err.println(attribution);
							}
						}

//						TODO: Extract date of quote e.g.   "Mar[ch] 9, 1999" and "[(][m]m/[d]d/[yy]yy[)]" (what about other forms e.g. dd/mm/yyyy dd mmm, yyyy etc? Add multiple checks as needed!)
//						TODO: Add "date of quote" and perhaps "year of quote" fields too, to supplement the existing yearBorn and yearDied fields.
						String regexMonthDDYear = "[,?!. ]{1,}[A-Z]{1}[a-z]{2,}[ ]+[0-9]{1,2}[, ]+[0-9]{4}";  //   Mon[thname] [0]9, 2011  - what about '11?
						String regexMM_DD_YYYY  = "[,?!. ]{1,}[0-9]{1,2}[-/]{1}[0-9]{1,2}[-/]{1}[0-9]{2,4}";  //  [m]m/[d]d/[yy]yy

						pattern = Pattern.compile(regexMonthDDYear);
						matcher = pattern.matcher(attribution);
						if (matcher.find()){
							dateOfQuote = matcher.group(); //TODO: this overwrites any extracted (yyyy) above; check overwrite before doing it?
							if (inDebugMode) System.err.println("Date of Quote:"+dateOfQuote);
							if (inDebugMode) System.err.println(attribution);
							attribution = attribution.replaceFirst(regexMonthDDYear,"");
							if (inDebugMode) System.err.println(attribution);
						}

						pattern = Pattern.compile(regexMM_DD_YYYY);
						matcher = pattern.matcher(attribution);
						if (matcher.find()){
							dateOfQuote = matcher.group(); //TODO: this overwrites any extracted (yyyy) above; check overwrite before doing it?
							if (inDebugMode) System.err.println("Date of Quote:"+dateOfQuote);
							if (inDebugMode) System.err.println(attribution);
							attribution = attribution.replaceFirst(regexMM_DD_YYYY,"");
							if (inDebugMode) System.err.println(attribution);
						}

						
//						Search for "*", count them and come up with an integer 1-5 as the starRating; No *'s returns 0; 0 means unrated.	
						String regexStars = "[ ,]{1,}[\\*]{1,}"; 
						pattern = Pattern.compile(regexStars);
						matcher = pattern.matcher(attribution);
						if (matcher.find()) {
							stars = attribution.substring( matcher.start()+1, matcher.end() ).trim();
							starRating = stars.length();
								
							if (inDebugMode) System.err.println(attribution);							
							attribution = attribution.replaceFirst(regexStars, "");							
							if (inDebugMode) System.err.println(attribution);
						
						}

//						 Extract 'quoted' strings as context field.  Also (parenthesized strings)
						String regexContext = "['(]{1}[A-Z a-z][)']{1}";
						pattern = Pattern.compile(regexContext);
						matcher = pattern.matcher(attribution);
						if (matcher.find()) {
							context = attribution.substring( matcher.start()+1, matcher.end()-1 );  // or matcher.group()
							if (inDebugMode) System.err.println(context);
							
							if (inDebugMode) System.err.println(attribution);
							attribution = attribution.replaceFirst(regexContext, "");
							if (inDebugMode) System.err.println(attribution);

						}
						
//						Extract NON-quoted/parenthesized strings after a comma or a colon as "vocation".
						String regexVocation = "[:,]{1}[^'(]{1}[A-Z, a-z][^)']{1}$";
						pattern = Pattern.compile(regexVocation);
						matcher = pattern.matcher(attribution);
						if (matcher.find()) {
							vocation = attribution.substring( matcher.start()+1, matcher.end()-1 );  // or matcher.group()
							if (inDebugMode) System.err.println(vocation);
							
							if (inDebugMode) System.err.println(attribution);
							attribution = attribution.replaceFirst(regexVocation, "");
							if (inDebugMode) System.err.println(attribution);

						}
						
					}
					
					if (inDebugMode) {
						System.err.println(quote);
						System.err.println(" " + attribution);
						System.err.println(" " + originalAttribution);
						System.err.println("");
					}

					if (inDebugMode) System.err.println("--------------------------------------------------------------------------------");
					
					quotes.add(new Quotation(quoteCount, quote, attribution, vocation, context, yearBorn, yearDied, starRating, dateOfQuote, originalAttribution));
					quoteCount++;
				}
			}
			buffer.close();
			quoteReader.close();								
		}
		catch (Exception e) 
		{
			System.err.println("Error processing line #" + lineCount + " of quote file '" + quoteFile.getAbsoluteFile()+ "'");
			System.err.println(" Error message: " + e.getMessage());
			e.printStackTrace();
			System.exit(0);
		}
		
		return quotes;
	}
	
	public static List<Quotation> handleCsvFile(File quoteFile) {
		
		String quoteLine = null;
		List<Quotation> quotes = new ArrayList<Quotation>();
		
		long lineCount  = 0L;
		long quoteCount = 0L;

		try 
		{
			FileReader quoteReader = new FileReader(quoteFile);
			BufferedReader buffer = new BufferedReader(quoteReader);

			while ((quoteLine = buffer.readLine()) != null)
			{
				lineCount++;
//				TODO add "if commands.contains("debug")" before doing the sytem.out.println below.
//				System.out.println(quote);
				Quotation quotation = parseQuote(quoteLine);
				quotes.add(quotation);
				quoteCount++;
			}
			buffer.close();
			quoteReader.close();
		}
		catch (Exception e) 
		{
			System.err.println("Error processing quote file '" + quoteFile.getAbsoluteFile()+ "'");
			System.err.println(" Error message " + e.getMessage());
			//System.exit(0);
			//TODO: throw file not found or other file i/o exceptions
		}
		
		return quotes;
		
	}

	
	public static List<Quotation> handleXmlFile(File quoteFile) {
//		TODO: Handle XML files created by Flat2XML project in this workspace.
		List<Quotation> quotes = new ArrayList<Quotation>();
		System.err.println("*.xml file format is not yet implemented; please try a different file.");
		return quotes;
	}

	
	public static List<Quotation> handleFlatFile(File quoteFile) {
		// TODO: Add support for flat file from Flat2XML project in this workspace? Copy code from Flat2XML. Make it's own class.
		List<Quotation> quotes = new ArrayList<Quotation>();
		System.err.println("*.flat file format is not yet implemented; please try a different file.");
		return quotes;
	}	

	public static Quotation parseQuote(String quoteToParse)
	{
		String [] quoteArray = {};

		Quotation quotation = null;

		try
		{
			//TODO: this does not deal with commas inside quoted strings.  Change to "\"," ?)
			//quoteArray = quoteToParse.split(",");
			//quoteArray = quoteToParse.split("\"[ ]*,");

			//(?:(?<=")([^"]*)(?="))|(?<=,|^)([^,]*)(?=,|$)
			// http://stackoverflow.com/questions/1441556/parsing-csv-input-with-a-regex-in-java
			
			//quoteArray = quoteToParse.split("(?:(?<=\")([^\"]*)(?=\"))|(?<=,|^)([^,]*)(?=,|$)");

			CSVParser myCSV = new CSVParser();
			quoteArray = myCSV.parse(quoteToParse);
        }
		catch (Exception e)
		{
			System.err.println("Error splitting quote into separate field:" + quoteToParse);
			System.err.println(" Error message:" + e.getMessage());
		}

		if (quoteArray.length != 8)
		{
			System.err.println("Skipping quote line; it may not be formatted properly or have all 8 fields, found " +quoteArray.length + " fields: " + quoteToParse);
			System.err.println("  Fields extracted:");
			for (int i=0; i < quoteArray.length; i++) 
			{
				System.err.println("    "+i+": " + quoteArray[i]);
			}
		}
		else
		{
			for (int i = 0; i < quoteArray.length; i++)
			{
				try
				{
//					TODO add debugging parameter so we can output this if desired.
//					System.out.println(i+": "+quoteArray[i]);
					quoteArray[i] = quoteArray[i].trim();
					quoteArray[i] = Trim.leadingAndTrailing(quoteArray[i],"\"");
//					System.out.println(i+": "+quoteArray[i]);
				}
				catch (Exception e)
				{
					System.err.println("Error parsing quote field #" + (i+1) + " in line: " + quoteToParse);
					System.err.println(" Error message:" + e.getMessage());
				}
			}
			
			try
			{
				  quotation = new Quotation(	Long.parseLong(quoteArray[0]),
												quoteArray[1],
												quoteArray[2],
												quoteArray[3],
												quoteArray[4],
												quoteArray[5],
												quoteArray[6],
												Integer.parseInt(quoteArray[7]),
												"",	// dateOfQuote
												""	// attribution
											);						
			}
			catch (Exception e)
			{
				System.err.println("Error adding quotation to list: " + quoteToParse);
				System.err.println(" Error message: " + e.getMessage());
			}			
		}
		
		return quotation;
	
	}


}
