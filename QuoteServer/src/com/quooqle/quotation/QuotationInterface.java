package com.quooqle.quotation;

interface QuotationInterface {


	// Common methods

	public String 		toString(); 					//TODO: do we need to define this here too, isn't it a standard Object method and so will be inherited?

	public String 		starsString();

	public String 		starsString( int starRating ); 	//TODO: Can we do this (overloading?)


}

