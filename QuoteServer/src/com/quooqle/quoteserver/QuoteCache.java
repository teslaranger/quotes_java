package com.quooqle.quoteserver;

import java.util.Collections;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.quooqle.quotation.Quotation;
import com.quooqle.quotation.QuoteFileReader;

/* 
 * This is a singleton class; only a single instance can be created, 
 * and that's done and cached by the class itself the first time it 
 * is called.  Use this class to cache items in memory that you do
 * not want to keep re-reading from disk. 
 */

public final class QuoteCache 
{
	private static QuoteCache quoteCache;


	private List<Quotation> quotations = Collections.synchronizedList( new ArrayList<Quotation>() );

	//Constructor
	private QuoteCache()
	{
		//TODO: Change to use of SQL database instead of in-memory data.
		//TODO: Figure out how to get file from inside deployed web app file structure."  use .getClass.getResource()?
		//TODO: Or create a properties file for this web app and get the path from there; see web warrants code for example.  
		//TODO: Store properties in a singleton class at web app load time
		
		String tmpDir = System.getProperties().getProperty("java.io.tmpdir");		
		System.out.println("java.io.tmpdir property:" + tmpDir + "\n");

		String usrDir = System.getProperties().getProperty("user.dir");		
		System.out.println("user.dir property:" + usrDir + "\n");
		
		System.out.println("catalina.base:" + System.getProperty("catalina.base"));
		System.out.println("catalina.base:" + System.getProperties().getProperty("catalina.base"));
		
		System.out.println("wtp.deploy:" + System.getProperty("wtp.deploy"));
		System.out.println("wtp.deploy:" + System.getProperties().getProperty("wtp.deploy"));	
		
		try {
			System.out.println("getAbsoluteFile:" +  new File("").getAbsoluteFile());
			System.out.println("getCanonicalPath1:" +  new File("").getCanonicalPath());
			System.out.println("getCanonicalPath2:" +  new File(".").getCanonicalPath()); 
		} catch (IOException e) {
			
		} finally {
			
		}
		//TODO: Move filespec to a .properties file (anything else? ANY literals? Get properties singleton class from old web warrants code?)
		
		//This works, at least locally. See what it does on JavaPipe (ANSWER: Doesn't work)
		
		//TODO: Change this to get a stream from a resource in /WEB-INF/quote.txt or /WEB-INF/quotes.txt
		quotations = QuoteFileReader.readQuotesFromFile(System.getProperty("wtp.deploy") + "/QuoteServer/WEB-INF/quotes/Quotes.txt");  	

		
		//quotations = QuoteFileReader.readQuotesFromFile("/Users/billd/eclipse-workspace/quotes/QuoteServer/WEB-INF/quotes/Quotes.txt");  //"D:/workspaces/quotes/QuoteServer/Quotes.txt");
		//quotations = QuoteFileReader.readQuotesFromFile(System.getProperty("catalina.base") + "/webapps/QuoteServer/WEB-INF/Quotes.txt"); 
		
		
		// Try this instead so it's in the hidden /WEB-INF folder and can't be accessed remotely.
		//quotations = QuoteFileReader.readQuotesFromFile(System.getProperty("wtp.deploy") + "/QuoteServer/WEB-INF/quotes/Quotes.txt"); 
	
		// on JavaPipe it's at /home/williamb/williambdavisjr/tomcat/webapps/QuoteServer/WEB-INF/quotes/Quotes.txt
		//quotations = QuoteFileReader.readQuotesFromFile("/home/williamb/williambdavisjr/tomcat/webapps/QuoteServer/WEB-INF/quotes/Quotes.txt"); 
		//quotations = QuoteFileReader.readQuotesFromFile("/home/williamb/williambdavisjr/apache-tomcat-8.5.23/webapps/QuoteServer/WEB-INF/quotes/Quotes.txt"); 
		
	}
	
	public static QuoteCache getInstance()
	{	
		synchronized(QuoteCache.class)
		{
			if (null == quoteCache)
			{
				quoteCache = new QuoteCache();
			}
		}
		return quoteCache;
	}
	
		
	public List<Quotation> getQuotations() 
	{
		return quotations;
	}
	
	public Quotation getQuotation(int n) 
	{
		return quotations.get(n);
	}

	//TODO: this is a duplicate of getQuotation, but better.
	public Quotation getNumberedQuotation(int r) 
	{
		if ( (r >= 0) && (r < quotations.size()) ) {		
			return quotations.get(r);
		} else {
			return null;
		}
	}

	public int getQuotationsLength() 
	{
		return quotations.size();
	}
		
	public int getRandomQuotationNumber() 
	{
		Random rnd= new Random();
		int r= rnd.nextInt(quotations.size()); //returns 0 to (quotes array length-1)
		return r;
	}

	public String getRandomQuotationString() 
	{
		return getNumberedQuotationString(getRandomQuotationNumber());
	}

	public Quotation getRandomQuotation() 
	{
		return quotations.get(getRandomQuotationNumber());
	}
	 
	public String getNumberedQuotationString(int r) 
	{
		if ( (r >= 0) && (r < quotations.size()) ) {
			return HtmlUtils.getHtmlFormattedQuotation(quotations.get(r), r, quotations.size());
 		} else {
			return "";
		}
	}

} // class
