package com.quooqle.quoteserver;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quooqle.fortune.Fortune;

/**
 * Servlet implementation class Fortune
 */
public class RandomFortune extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RandomFortune() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter(); 
		response.setContentType("text/html"); 
		
		out.println("<html>");
		out.println("	<head>");
		out.println("		<meta HTTP-EQUIV='Refresh' CONTENT='10; URL=http://localhost:8080/QuoteServer/RandomFortune'>");
		out.println("	</head>");
	    out.println("	<body onclick='location.reload(true)' ondblclick='location.reload(true)' onkeypress='location.reload(true)'>");
		out.println(new Fortune().get());
		out.println("	</body>");
		out.println("</html>");

		out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
