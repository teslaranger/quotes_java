package com.quooqle.quoteserver;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class QuoteSearch
 */
public class QuoteSearch extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QuoteSearch() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		response.setContentType("text/html");

		String searchString = request.getParameter("searchstring");

		if (searchString != null) {
			System.err.println("QuoteSearch.doGet(): searchString="
					+ searchString);
			doSearch(out, request);
		} else {
			out.println(formatSearchResultsHeader());

			out.println(HtmlUtils.displayQuoteSearchForm(searchString));

			out.println(formatSearchResultsFooter());

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		System.err.println("QuoteSearch.doPost()");

		doSearch(out, request);

	}

	// TODO: Move to com.quoogle.util.ServletUtils.java
	private String getParameterString(HttpServletRequest request,
			String parameterToGet, String defaultValue) {

		String result = defaultValue;

		try {
			result = request.getParameter(parameterToGet);
			if (result == null) {
				result = defaultValue;
			}
		} catch (NullPointerException e) {
			// parameter not provided, used default
			result = defaultValue;
		} catch (Exception e) {
			// unexpected error, use default
			System.err.println("Unable to get parameter " + parameterToGet
					+ ", using default value: '" + defaultValue + "', error:"
					+ e.getMessage());
			result = defaultValue;
		}

		return result;
	}

	// TODO: Move to com.quooqle.util.ServletUtils.java
	private boolean getParameterBoolean(HttpServletRequest request,
			String parameterToGet, boolean defaultValue, String defaultString) {

		boolean result = defaultValue;
		String parameter = null;

		try {
			parameter = request.getParameter(parameterToGet);

			if (parameter == null) {
				result = defaultValue;
			}

			if (parameter.equalsIgnoreCase(defaultString)) {
				result = true;
			}

			if (parameter.equalsIgnoreCase("true")) {
				result = true;
			}
			if (parameter.equalsIgnoreCase("yes")) {
				result = true;
			}
			if (parameter.equalsIgnoreCase("y")) {
				result = true;
			}
			if (parameter.equalsIgnoreCase("on")) {
				result = true;
			}

			if (parameter.equalsIgnoreCase("false")) {
				result = false;
			}
			if (parameter.equalsIgnoreCase("no")) {
				result = false;
			}
			if (parameter.equalsIgnoreCase("n")) {
				result = false;
			}
			if (parameter.equalsIgnoreCase("off")) {
				result = false;
			}
		} catch (NullPointerException e) {
			// parameter not provided, use default
			result = defaultValue;
		} catch (Exception e) {
			// unexpected error, use default
			System.err.println("Unable to get parameter " + parameterToGet
					+ ", using default value: '" + defaultValue + "', error:"
					+ e.getMessage());
			result = defaultValue;
		}

		return result;
	}

	private void doSearch(PrintWriter out, HttpServletRequest request) {

		// Request Parameter defaults
		String searchString = "Unknown";
		String searchMethod = "eachword";
		String quotePersonBoth = "both";
		String caseSensitive = "no";
		boolean graybar = true;

		searchString = getParameterString(request, "searchstring", searchString); // DO
																					// NOT
																					// make
																					// this
																					// lower
																					// case
																					// like
																					// other
																					// params;
																					// would
																					// break
																					// "case sensitive"
																					// param.
		searchMethod = getParameterString(request, "searchmethod", searchMethod)
				.toLowerCase();
		quotePersonBoth = getParameterString(request, "quotepersonboth",
				quotePersonBoth).toLowerCase();
		caseSensitive = getParameterString(request, "casesensitive",
				caseSensitive).toLowerCase();
		graybar = getParameterBoolean(request, "graybar", graybar, "true");

		String result = HtmlUtils.searchForQuotation(searchString,
				searchMethod, quotePersonBoth, caseSensitive, graybar);

		out.println(formatSearchResultsHeader());
		out.println(HtmlUtils.displayQuoteSearchForm(searchString));
		out.println(formatSearchResults(searchString, result));
		out.println(formatSearchResultsFooter());
	}

	public String formatSearchResultsHeader() {
		// TODO Move this to using a template system like we do mail in income
		// offset
		String header = "<html>"
				+ "	<head>"
				+ "		<title>Quooqle: Quote search results</title>"
				+ "	</head>"

				+ "	<body onload='document.quotesearch.searchstring.focus();'>"
				+ "		<h1 style='font-family: \"Arial Black\", sans-serif; '><b>"
				+ HtmlUtils.getLogoHtml(1) + " search results</b></h1> "
				+ "			<h4>What do you want a quote about today?</h4>" + "			";

		return header;
	}

	public String formatSearchResults(String searchString, String result) {
		// TODO Move this to using a template system like we do mail in income
		// offset

		String searchResult = "		<p><hr/>"
				+ "		<b>Results of search for &lsquo;" + searchString
				+ "&rsquo;:</b>" + "		<hr/></p>"

				+ "		<ul>" + "		" + result + "		</ul>";

		return searchResult;
	}

	public String formatSearchResultsFooter() {
		// TODO Move this to using a template system like we do mail in income
		// offset

		String footer = "		<p><hr/></p>" + "		</body>" + "</html>";

		return footer;
	}

} // class
