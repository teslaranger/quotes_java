package com.quooqle.quoteserver;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DisplayQuote
 */
public class DisplayQuote extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayQuote() {
    
    	super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter(); 
		response.setContentType("text/html");
		
		String num=request.getParameter("number");
		int n = 0;

		if (num != null && num.length(  ) != 0) 
		{
			try 
			{
				n = Integer.parseInt(num);  //TODO: n should never be larger than quotes.length but should check for 0...quotes.length-1 and handle out-of-range.
				n = n -1 ; // because we show them as 1-n but they are stored 0-(n-1)
				HtmlUtils.generateFancyQuotationPage(out, n);	
			} 
			catch (NumberFormatException e) 
			{
				//TODO: handle error better;
			}
		}	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
