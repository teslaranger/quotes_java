package com.quooqle.quoteserver;

import java.io.*; 

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class QuoteServlet
 */
public class RandomQuote extends HttpServlet 
{
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RandomQuote() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		PrintWriter out = response.getWriter(); 
		response.setContentType("text/html"); 		

		String searchString = null;
		
		try {
			searchString = request.getParameter("searchstring");
		}
		catch (NullPointerException e) {
			// parameter not provided, just get any old random quote
		}
		catch (Exception e) {
			// unexpected error, use default
			System.err.println("RandomQuote.doGet():Unable to get parameter searchstring, error:" + e.getMessage());			
		}
				
		if (searchString == null) {
			HtmlUtils.generateFancyQuotationPage( out, QuoteCache.getInstance().getRandomQuotationNumber()  );
		} else {
			//search for searchstring in all fields, display random result from all found quotes that match search string.
			int r = HtmlUtils.searchForRandomQuotationString(searchString);
			HtmlUtils.generateFancyQuotationPage( out, r  );
		}
	 	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			//TODO: Add code here to take a parameter and generate a random quote on a the contents of that parameter; get a list of matches and pick one randomly.
	}
	

}