package com.quooqle.quoteserver;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quooqle.quotation.Quotation;

/**
 * Servlet implementation class GetQuoteAjax
 */
public class GetRandomQuote extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetRandomQuote() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//		response.setContentType("text/plain");	
		response.setContentType("text/html");	//either text/html or text/plain work when inserted into an HTML page, but text/html is nicer when testing.
		response.setCharacterEncoding("UTF-8");

		String quote = "";

		//TODO: Add ?number=n support to get a specifically numbered quote all nicely but simply formatted?
		//TODO: if we get more than one parameter, decide which one takes priority.
		//TODO: Make a version of this servlet that just returns plain text or takes a &format=text|html|xml|??? parameter. Default to &format=text.  textsingleline, textdoubleline or textmultiline

		String searchString = request.getParameter("searchstring");
//		String dateString   = request.getParameter("date");
//
//		System.err.println("date param:"+dateString);
//		String date = "";
//		
//		if (dateString!=null) {
//			if (dateString.length()== 8) {
//				String year  = dateString.substring( 0, 3 );
//				String month = dateString.substring( 4, 5 );
//				String day   = dateString.substring( 6, 7 );
//				date = month + "/" + day + "/" + year;
//				
//				// search quote attribution (and eventually quoteDate and birthDate fields) and build a list of quotations; 
//				// when done, pick one at random and return it. 
//				// model on HtmlUtils.searchForRandomQuotationString() but pass date instead.
//				
//				int quoteNum = HtmlUtils.searchForRandomDatedQuotationString(date);
//
//				if (quoteNum == 0) {
////					quote = "<i>&ldquo;This quotation intentionally left blank.&rdquo;</i><br/>&nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;Unknown"; 
//					quote = "&ldquo;This function has not been implemented yet. Sorry!&rdquo;<br/>&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;Bill Davis";
//				} else { 
//					Quotation q  = QuoteCache.getInstance().getNumberedQuotation(quoteNum);
//					quote = "<i>&ldquo;" + q.getText() + "&rdquo;</i><br/>&nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;" + q.getPerson();
//					//TODO: date "born: mm/dd/yyyy" or "said: mm/dd/yyy" to end in parentheses
//				}
//				
//			} else {
//				quote = "Invalid date '" + dateString + "', must be formatted yyyymmdd."; 
//			} 
//		} else {
//			quote = "Missing date, must be yyyymmdd.";
//		}
//		
//
//		if ( !quote.equals("") ) {
//			quote = insertLineBreaks(quote);
//			System.err.println(quote);
//			response.getWriter().write(quote);
//			return;
//		}
		
		
		// See if there was a ?searchstring= on the URL; if so, 
		if ( (searchString==null) || (searchString.equals("")) ) {
			Quotation q = QuoteCache.getInstance().getRandomQuotation();			
			quote = "<i>&ldquo;" + q.getText() + "&rdquo;</i><br/>&nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;" + q.getPerson(); 
		} else {
			int quoteNum = HtmlUtils.searchForRandomQuotationString(searchString);
			if (quoteNum == 0) {
				quote = "<i>&ldquo;This quotation intentionally left blank.&rdquo;</i><br/>&nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;Unknown"; 
			} else { 
				Quotation q  = QuoteCache.getInstance().getNumberedQuotation(quoteNum);
				quote = "<i>&ldquo;" + q.getText() + "&rdquo;</i><br/>&nbsp;&nbsp;&nbsp;&nbsp;&mdash;&nbsp;&nbsp;" + q.getPerson(); 
			}
		}
		
		quote = insertLineBreaks(quote);
		System.err.println(quote);
		response.getWriter().write(quote);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

	private String insertLineBreaks(String quote) {
				
		String regex="\\|";	//Note: | is a regex meta char, so it must be escaped if you want to search FOR that character. In a string, to insert a \ you must type \\, or for \\ use \\\\.

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(quote);
		
		int count = 0; 
		while (matcher.find()) { 
			count++; 
		}

		if (count > 0) {
			quote = quote.replaceAll(regex,"<br/>\n");
		}
		
		return quote;
	}
	
}


