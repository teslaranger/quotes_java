package com.quooqle.quoteserver;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import com.quooqle.quotation.Quotation;
import com.quooqle.util.Number;

public class HtmlUtils {

	public static String displayQuoteSearchForm() {

		return displayQuoteSearchForm("");
		
	}
	
	public static String displayQuoteSearchForm(String stringToSearchFor) {
		
		if ( (stringToSearchFor == null) || (stringToSearchFor.equals("")) ) {
			stringToSearchFor = "Type what you want to search for here...";
		}

		//TODO Move this to using a template system like we do for emails sent by in income offset system at State?
		
		String form = "		<form name='quotesearch' method=post action='/QuoteServer/QuoteSearch'>\n"
					+ "			<input type='text' name='searchstring' size=75 value='" + stringToSearchFor + "' tabindex=1> \n"
					+ "			&nbsp; <a href='#' tabindex=2 style='font-size: x-small; ' onclick='return toggleVis(\"options\");'>Advanced search...</a> &nbsp; \n"
					+ "			<span id='searching'>&nbsp;</span> &nbsp; &nbsp; \n"
					+ "			<img  id='spinner' name='spinner' width=16 height=16 border=0 src=images/spacer.gif >\n"
					+ "			<p>\n"
					+ "			<input type='button' value='Find a quotation!' tabindex=3 "
					+ "                onclick='document.quotesearch.submit(); \n"
					+ "                         document.getElementById(\"spinner\").src=\"images/spinner.gif\"; \n"
					+ "                         document.getElementById(\"searching\").innerHTML=\"<b><blink>Searching...</blink></b>\"; '>\n"
					+ "			<button type=button tabindex=4 onclick='locn=\"/QuoteServer/RandomQuote?searchstring=\"+document.quotesearch.searchstring.value; location=locn; '>I Feel Fortunate!</button>\n"
					+ "			</p>\n"
					+ "			<div id='options' style='display: none; background-color: lavender; '> \n"
					+ "				<p>Search for:\n"
					+ "					<input type='radio' name='searchmethod' value='wholephrase' tabindex=5 >Whole phrase exactly\n"
					+ "					<input type='radio' name='searchmethod' value='eachword'    tabindex=6 checked >Each word separately\n"
					+ "					<input type='radio' name='searchmethod' value='and'         tabindex=7 disabled ><span style='color: gray; '>AND word search</span>\n"
					+ "					<input type='radio' name='searchmethod' value='or'          tabindex=8 disabled ><span style='color: gray; '>OR word search</span>\n"
					+ "				</p>\n"
					+ "				<p>Search for text in: \n"
					+ "					<input type='radio' name='quotepersonboth' value='both'   tabindex=9 checked>Both quote and name\n"
					+ "					<input type='radio' name='quotepersonboth' value='quote'  tabindex=10>Quote only\n"
					+ "					<input type='radio' name='quotepersonboth' value='person' tabindex=11>Person's name only\n"
					+ "				</p>\n"
					+ "				<p>Search is: \n"
					+ "					<input type='radio' name='casesensitive' value='yes' tabindex=12>Case sensitive\n"
					+ "					<input type='radio' name='casesensitive' value='no'  tabindex=13 checked>Case insensitive\n"
					+ "				</p>\n"					
					+ "				<p>Show quotes in graybar\n"
					+ "					<input type='radio' name='graybar' value='on'  tabindex=14 checked>On\n"
					+ "					<input type='radio' name='graybar' value='off' tabindex=15>Off\n"
					+"				</p>\n"
					+ "			</div>\n"
					+ "			<script src='quoteutils.js' type='text/javascript' language='javascript'></script>\n"
					+ "		</form>\n"
					+ "     <script type=\"text/javascript\" language=\"JavaScript\">\n"
					+ "			document.forms['quotesearch'].elements['searchstring'].focus();\n"
					+ "			document.forms['quotesearch'].elements['searchstring'].select();\n"	
					+ " 	</script>\n";
					
					return form;
	}	


	/**
	 * We have a number of numbered style sheets and change them randomly each time a quote is displayed.
	 */
	public static String getRandomStyleSheetName() 
	{
		//TODO: Use File.exists() method to check for "randomcolorsN.css" filenames 0-N until we can't find one. Use that for numStyleSheets. 
		int numStyleSheets = 5;		

		//int style = (int)(Math.random()*numStyleSheets);	    	 
		Random rnd = new Random();
		int style = rnd.nextInt(numStyleSheets);   // returns 0..(numStyleSheets-1)
		  
		//style=4;
		    
		return "randomcolors" + style + ".css";
	}


	public static String getHtmlFormattedQuotation(Quotation quotation, int quoteNumber, int numberOfQuotes) 
	{
		//TODO: Move the following code to a routine that returns an HTML-formatted quote given a specific quote number.	

		String quote  = quotation.getText();
		String person = quotation.getPerson();
		
//		System.out.println("Quote #" + (quoteToDisplay+1) + " of " + quotes.length);
//		System.out.println(quote);
//		System.out.println(person);
//		System.out.println("");
		
		String openQuote  = "<span class='leftQuote'><sup>&ldquo;</sup></span>";
		String closeQuote = "<span class='rightQuote'><sup>&rdquo;</sup></span>";
		quote = openQuote + quote + closeQuote;

		//Loop through string and replace | or \ found with <ul><li> and keep a count of indent level so we can output that many </ul>s. On 2nd-nth, add "</li>" before <ul><li> 
		//e.g.     "Manifest plainness, | Embrace simplicity, | Reduce selfishness, | Have few desires."
		// becomes:"Manifest plainness, <ul><li> Embrace simplicity, </li><ul><li>Reduce selfishness, </li><ul><li>, Have few desires.</ul></ul></ul>"

		//String regex = "[\\\\|\\|]"; //search for | or \
		String regex="\\|";	//Note: | is a regex meta char, so it must be escaped if you want to search FOR that character. In a string, to insert a \ you must type \\, or for \\ use \\\\.

		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(regex);
		java.util.regex.Matcher matcher = pattern.matcher(quote);
		
		int count = 0; 
		while (matcher.find()) { 
			count++; 
		}

		// make an indented list out of the lines of the quote marked with "|"
//		if (count > 0) {
//			quote = "<ul><li>" + quote;  
//			//quote=quote.replaceFirst(regex, "</li><ul><li>");
//			quote = quote.replaceAll(regex,"</li>\n<ul><li>");
//			quote = quote + "</li>\n";
//			for (int i = 0; i < count; i++){
//				quote = quote + "</ul>\n";
//			}				
//			quote = quote + "</ul>";
//		}

		if (count > 0) {
			quote = quote.replaceAll(regex,"<br/>\n");
		}

		//TODO: Other option; loop through "|" and indent each line by one more space e.g.
		//   Line One
		//    Line Two
		//     Line Three
		
		//TODO: let user control the style of multiline quotes?  Indented/straight/etc?
		
		String quoteNumberInfo = "<p class='quoteNumber' style='text-align: left; color:silver; font-size: small; '>Quote #" + (quoteNumber+1) + " of " + numberOfQuotes + "</p>";
		
		String result = "	<script>document.getElementById(\"top\").innerHTML=\"" + quoteNumberInfo + "\";</script>"
					  + "   <noscript><p>" + quoteNumberInfo + "</p></noscript>"
					  +	"	<p>&nbsp;</p>" //or put "quoteNumber" variable here.
					  + "	<p><span class='quote'>" + quote + "</span></p>"  
		              + "	<cite class='quoteCite'>&nbsp;&mdash;&nbsp;" + person + "</cite>";	
		       			//TODO: put the &mdash in a cite:before?  Not compatible with all browsers but lets us change styles easier/randomly			
		return result;				
	}

	public static void generateFancyQuotationPage(PrintWriter out, int quoteNumber) {
		// start of template HTML part 1

		QuoteCache qc = QuoteCache.getInstance();
		
		//out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		//out.println("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
		out.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">");
		out.println("<html>");
		out.println("	<head>");
	    out.println("		<title>Quooqle: Your Random Quote</title>");	//TODO: Make the title a parameter
	    out.println("		<meta HTTP-EQUIV='Refresh' CONTENT='10; URL=http://localhost:8080/QuoteServer/RandomQuote'>");
	    out.println("		<link rel='stylesheet' href='RandomQuote.css' />");

	    //TODO: Let user select style sheet, see CSS Zen Garden? Associate quotes with specific style sheets?
	    //TODO: Associate font, size, style, colors WITH the quote not the style sheet?  
	    
	    out.println("		<link rel='stylesheet' href='" + HtmlUtils.getRandomStyleSheetName() + "' />");	    	   
	    out.println("	</head>");
		
	    out.println("	<body ondblclick='location.reload(true)' onkeypress='location.reload(true)'>");	//TODO: Make the JS a parameter

	    // web page content
		out.println("			<div id='top'>");
		out.println("				<h2></h2>");
		out.println("			</div>"); 			
		
		out.println("			<div id='floater'></div>");

		out.println("			<div id='content' onclick='location.reload(true)'>");
		out.println("				<p>" + HtmlUtils.getHtmlFormattedQuotation(qc.getNumberedQuotation(quoteNumber),quoteNumber, qc.getQuotationsLength())); 
		out.println("				</p>");		
		out.println("			</div>");
		// end of web page content
		
		out.println("		<div class='push\'></div>");        //TODO: is this needed any more?
		
        out.println("		<div class='footer\'");	
		out.print("			<p style='position:fixed; bottom:-1em; left:0px; color: black; background-color: lavender; '> Courtesy of " + getLogoHtml(1) + " &nbsp; &mdash; &nbsp; &nbsp; Press any key or click the mouse on the quote to see another quote.");
		out.print("				&nbsp; <a href='index.jsp'>Home</a> &nbsp;|&nbsp; <a href='/QuoteServer/QuoteSearch'>Search</a> &nbsp;|&nbsp; Random ");
//		out.print("			<button class='NewQuoteButton' tabindex='1; accesskey='R' onclick='location.reload(true)'>Another <u>R</u>andom quote&nbsp;&nbsp;(Alt+R)</button>");
//																	use accesskey=&#x20 to make Alt+Space be the shortcut key for the button. No way to make JUST space, that i can see. 
		out.println("		</p>");
		//TODO: Make this message a parameter: pass in "..to see another quote" or "..to see the quote in a different style" or whatever.
		//out.println("		<p style='position:fixed; bottom:0px; right:0px; color: black; background-color: lightgrey; '>Press any key or click mouse on quote to see another quote.</p>");
		out.println("		</div>");
	    
		out.println("	</body>");
	    out.println("</html>");	    
	}

	
	public static int searchForRandomDatedQuotationString(String date) {
		
		int result = 0;
		
		//TODO: add code here from searchForRandomQuotationString() modified to search attribution field and also ADD quoteDate and birthdate fields to Quotation object/interface.
		
		return result;
	}
	
	// Search all quotations for specified string, in both text and person fields. Return id of one of the matches randomly.
	public static int searchForRandomQuotationString(String searchTerms) {
		
		int result = 0;
		
		List<String> searchStrings = new ArrayList<String>();
		String[] stringsToFind = searchTerms.split(" ");
		searchStrings = Arrays.asList(stringsToFind);
		
//		System.err.print("searchForRandomQuotationString: String to search for:");
		for (String s: searchStrings) { 
			System.err.println(s);
		}
		
		int numQuotes = QuoteCache.getInstance().getQuotationsLength();
		List<Quotation> quotesFound = new ArrayList<Quotation>();
		
		for (int i=0; i<numQuotes; i++) {
			Quotation q = QuoteCache.getInstance().getQuotation(i);
//			System.err.println("Checking quote: '" + q.getText() + "' -" + q.getPerson());
			
			for (String s: searchStrings) {
				String slc = s.toLowerCase().trim();
				if (q.getText().toLowerCase().contains(slc) 
				||  q.getPerson().toLowerCase().contains(slc) ) {
//					System.err.println(" ^^^ MATCH ^^^");
					boolean added = false;
					try {
						added = quotesFound.add(q);
						if (added) { 
							// added successfully, do nothing
//							System.err.println("Found quote #" + quotesFound.size() + " matching:" + searchTerms );
//							System.err.println("  '" + q.getText()+"'  -" + q.getPerson());
						} else {
							// duplicate found, ignore error and quote
							System.err.println("Ignoring duplicate of quote already found matching: " + searchTerms);
							System.err.println("  '" + q.getText()+"'  -" + q.getPerson());
						}
					} catch(Exception e) {
						// exception adding, move on to checking next quote and ignore error
						System.err.println("Ignoring exception '" + e.getMessage() + "' when searching for random string matching" + searchTerms);
					}					
					break; // once part of quote matches, skip to next quote.
				}
			}
		}

		System.err.println(quotesFound.size()+ " quotes found matching:" + searchTerms);
		
		if (quotesFound.size() > 0) {
			Random r = new Random();
			int rq = r.nextInt(quotesFound.size());
			result = (int) quotesFound.get(rq).getId();
		}  
		
//		System.err.println("searchForRandomQuotationString result:" + result);

		return result;  // returns 0th quote if no result found.
		
	}
	

	//TODO: Add the search results to a new List<Quotation> and use that when producing the HTML output so we can paginate the search results? 
	public static String searchForQuotation(String searchTerms, String searchMethod, String quotePersonBoth, String caseSensitive, boolean graybar) 
	{
		String result = "";

		// Split this quote into an array or list using .split(" ") and search for each term separately 
		//UNLESS it's in double-quotes, then search for the string after stripping the quotes.

		//TODO: strip punctuation?  Or at least quotes, commas, semicolons, periods, exclamation, question mark, parens, colons?
		List<String> searchStrings = new ArrayList<String>();
		
		System.out.println("Seaching for:" + searchMethod);
		if (searchMethod.equalsIgnoreCase("eachword")) {
			//TODO: if any part of searchTerms contains quotes, split that out as a SINGLE search term (and strip the quotes when adding to the List) and remove it from the string before splitting the rest.
			String[] stringsToFind = searchTerms.split(" ");
			searchStrings = Arrays.asList(stringsToFind);  // does this ADD to searchStrings or replace?
		} else {
			// "wholephrase" search selected, put whole phrase in ONLY string in List of search strings; that way we can use same code to search for whole phrase or individual words
			searchStrings.add(searchTerms); 
		}

		System.out.println("Searching for string(s):");
		for (String s: searchStrings) {
			System.out.println(s);
		}
		
		//TODO: move to a stylesheet: searchResults.css 
		String light  = "style='background-color: white; '"; //#FFE4C4=bisque;	#F5F5F5  #DCDCDC  #D4D4D4 or lightgrey; #C0C0C0 or silver; #A9A9A9 or darkgray; #808080 or gray; #69696 or dimgray 
		String dark   = "style='background-color: lavender; '"; //#DEB887=burlywood
		String style  = dark;
		
		int count = 0;
				
		List<Quotation> quotes = QuoteCache.getInstance().getQuotations();
		
		System.out.println("Quote count"+quotes.size());
		
		for (int i=0; i < quotes.size(); i++) {

			// loop through List of search strings and check for each one in the quote/
			for (String searchString : searchStrings ) 
			{
				String quoteToCheck = quotes.get(i).getText().trim();

				//TODO: Add a method to generate a <li> for a quote; we use a similar style in QuoteSearch, FindDuplicateQuotes and ListQuotes.  Perhaps leave off the </li> since it is not actually required and lets someone add to the basic list item
				String person = quotes.get(i).getPerson().trim();
				String encodedPerson;
				try {
					encodedPerson = java.net.URLEncoder.encode(person, "UTF-8"); 
				} catch (UnsupportedEncodingException e) {
					encodedPerson = "";
				}	

				boolean found = false;

				if ( (quotePersonBoth.equalsIgnoreCase("quote")) || (quotePersonBoth.equalsIgnoreCase("both")) ) {
					if (caseSensitive.equalsIgnoreCase("yes")) {
						if (quoteToCheck.contains(searchString)) found = true;						
					} else {
						if (quoteToCheck.toLowerCase().contains(searchString.toLowerCase())) found = true;
					}
				}
				
				if ( (quotePersonBoth.equalsIgnoreCase("person")) || (quotePersonBoth.equalsIgnoreCase("both")) ) {
					if (caseSensitive.equalsIgnoreCase("yes")) {
						if (person.contains(searchString)) found = true;
					} else {
						if (person.toLowerCase().contains(searchString.toLowerCase())) found = true;
					}
				}
				
				if (found) 
				{
					if (graybar) { 
						if ( Number.isEven(count)) 	{ style = dark; }
						else 						{ style = light; }
					}
					
					result =  result + "<li " + style + "> " 
							+ "<a href='/QuoteServer/DisplayQuote?number=" + (i+1) + "'>#" + (i+1) + "</a>: " //TODO: change this to separate servlet /QuoteServer/GetQuote?quote=n
							+ "<b>&ldquo;" + quotes.get(i).getText() + "&rdquo;</b><br/>"
							+ "<cite>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &mdash; &nbsp;" 
							+ "<a href='/QuoteServer/QuoteSearch"
							+ "?searchstring=" +  encodedPerson
							+ "&quotepersonboth=person"
							+ "&searchmethod=wholephrase"
							+ "&graybar=true"
							+ "&casesensitive=false"
							+ "\'>"
							+ person 
							+ "</a></cite></li><br/>"; //TODO: Put a parameterized search for the person in quotes[i][1] as a hotlink 

					count++;
					break; // found at least one match, move on to next quote instead of searching for more matches.
				}
			}
		}
		
		if (count == 0) {
			result = "<p style='color: red; font-weight: bold; '>No matches for '" + searchTerms + "' in " + quotes.size() + " quotes searched.</p>";
		} else {
			result = "<p>Found " + count + " matches in " + quotes.size() + " quotes searched.</p>" + result;
		}
		
		return result;		
	}

	public static String QuoteMenu(PrintWriter out) {
		
		String result =   "	<form name=menuform'>\n"
						+ "		<select name='menu' onChange='top.location.href = this.form.menu2.options[this.form.menu2.selectedIndex].value; return false;'>\n"
						+ "			<option value='/index.jsp' selected>Home Page</option>\n"
						+ "			<option value='/QuoteServer/QuoteSearch'>Quote Search</option>\n"
						+ "			<option value='QuoteServer/RandomQuote'>Random Quote</option>\n"
						+ "			<option value='QuoteServer/RandomFortune'>Random Fortune</option>\n"
						+ "		</select>\n"
						+ "	</form>\n";
		
		return result;
	}
	
	public static String getLogoHtml() {
		return getLogoHtml(4);
	}
	
	public static String getLogoHtml(int ems) {
		//TODO: Too much repetitive code; refactor
//		return 	"<span style='color: green;      font-family: \"Arial Black\", Palatino, \"Times New Roman\", serif; font-weight: bold; font-size: " + ems + "em;'>&ldquo;&nbsp;</span>"
//			+ 	"<span style='color: blue;       font-family: cursive, \"Time New  Roman\", serif; font-weight: bold; font-size: " + ems + "em;'>Q</span>"
//			+	"<span style='color: red;        font-family: cursive, \"Time New  Roman\", serif; font-weight: bold; font-size: " + ems + "em;'>u</span>"
//			+	"<span style='color: goldenrod;  font-family: cursive, \"Time New  Roman\", serif; font-weight: bold; font-size: " + ems + "em;'>o</span>"
//			+	"<span style='color: green;      font-family: cursive, \"Time New  Roman\", serif; font-weight: bold; font-size: " + ems + "em;'>o</span>"
//			+	"<span style='color: blue;       font-family: cursive, \"Time New  Roman\", serif; font-weight: bold; font-size: " + ems + "em;'>q</span>"
//			+	"<span style='color: goldenrod;  font-family: cursive, \"Time New  Roman\", serif; font-weight: bold; font-size: " + ems + "em;'>l</span>"
//			+	"<span style='color: red;        font-family: cursive, \"Time New  Roman\", serif; font-weight: bold; font-size: " + ems + "em;'>e</span>"
//			+	"<span style='color: blue;       font-family: \"Arial Black\", Palatino, \"Time New  Roman\", serif; font-weight: bold; font-size: " + ems + "em;'>&nbsp;&rdquo;</span>";

		return 	"<span id='logo_oq' style='color: green; font-family: \"Arial Black\", Palatino, \"Times New Roman\", san-serif; font-weight: bold; font-size: " + ems + "em;'>&ldquo;&nbsp;</span>"
  			+ 	"<span id='logo'    style='color: blue;  font-family: cursive, serif; font-weight: bold; font-size: " + (ems+1) + "em;'>Q</span>"
  			+	"<span id='logo'    style='color: green; font-family: cursive, serif; font-weight: bold; font-size: " + ems + "em;'>u</span>"
  			+	"<span id='logo'    style='color: blue;  font-family: cursive, serif; font-weight: bold; font-size: " + ems + "em;'>o</span>"
  			+	"<span id='logo'    style='color: green; font-family: cursive, serif; font-weight: bold; font-size: " + ems + "em;'>o</span>"
  			+	"<span id='logo'    style='color: blue;  font-family: cursive, serif; font-weight: bold; font-size: " + ems + "em;'>q</span>"
  			+	"<span id='logo'    style='color: green; font-family: cursive, serif; font-weight: bold; font-size: " + ems + "em;'>l</span>"
  			+	"<span id='logo'    style='color: blue;  font-family: cursive, serif; font-weight: bold; font-size: " + ems + "em;'>e</span>"
  			+	"<span id='logo_cq' style='color: green; font-family: \"Arial Black\", Palatino, \"Time New  Roman\", san-serif; font-weight: bold; font-size: " + ems + "em;'>&nbsp;&rdquo;</span>";

	}
	
}
