package com.quooqle.fortune;

public class Fortune {

	   private String[] fortunes =
	   {
	      "This is an ignore, please test...",
	      "An unbreakable toy is useful for breaking other toys",
	      "What is another word for Thesaurus?",
	      "Rule 1: Don't tell people everything you know. Rule 2: ***  intentionally left blank  ***",
	      "Don't test for errors you can't handle.",
	      "Experience is something you don't get until just after you need it.",
	      "I think the best sign of intelligent life in the Universe is that no-one has tried to contact us.",
	      "Old MacDonald had an agricultural real estate tax abatement.",
	      "It is not possible to both understand and appreciate Intel CPUs.",
	      "The generation of random numbers is too important to be left to chance.",
	      "Don't you just hate rhetorical questions?"
	   };

	   private java.util.Random random = new java.util.Random();

	   public String get(int index)
	   {
	      return fortunes[index];
	   }

	   public String get()
	   {
//		   return fortunes[random.nextInt() % fortunes.length];
		   int index = random.nextInt(fortunes.length);
		   String fortune = "#" + (index+1) + " " + fortunes[index];
		   return fortune;
	   }
	
}