import java.awt.*;
//import java.awt.Event;
import java.awt.event.*;
import java.awt.datatransfer.Clipboard;
//import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
//import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;

import javax.swing.*;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BoxView;
import javax.swing.text.ComponentView;
import javax.swing.text.Element;
import javax.swing.text.IconView;
import javax.swing.text.LabelView;
import javax.swing.text.ParagraphView;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;

import java.net.URL;

import java.util.List;
import java.util.Random;

import java.io.*;

import com.williambdavisjr.quotation.*;

public class QuotesApp 
  implements Runnable, ActionListener
{
  	private JFrame   frame;
  	private JMenuBar menuBar;

  	private JMenu     fileMenu;
  	private JMenuItem fileNewMenuItem;
  	private JMenuItem fileOpenMenuItem;
  	private JMenuItem fileSaveMenuItem;
  	private JMenuItem fileSaveAsMenuItem;
  	private JMenuItem fileQuitMenuItem;

  	private JMenu     editMenu;
  	private JMenuItem editUndoMenuItem;
  	private JMenuItem editCutMenuItem;
  	private JMenuItem editCopyMenuItem;
  	private JMenuItem editPasteMenuItem;
  	private JMenuItem editClearMenuItem;
  	private JMenuItem editSelectAllMenuItem;

  	private JMenu     quoteMenu;
  	private JMenuItem quoteFindMenuItem;
  	private JMenuItem quoteFindNextMenuItem;
  	private JMenuItem quoteFirstQuoteMenuItem;
  	private JMenuItem quotePrevQuoteMenuItem;
  	private JMenuItem quoteNextQuoteMenuItem;
  	private JMenuItem quoteLastQuoteMenuItem;
  	private JMenuItem quoteNumberedQuoteMenuItem;
  	private JMenuItem quoteRandomQuoteMenuItem;

  	private JMenu     	optionsMenu;
  	private JMenuItem 	optionsPreferencesMenuItem;

  	private JMenu     	helpMenu;
  	private JMenuItem 	helpAboutMenuItem;

  	private JToolBar 	toolbar;
	
  	private JButton 	tbRandomButton;
  	private JButton 	tbFirstButton;
	private JButton 	tbPrevButton;
	private JButton 	tbNextButton;
	private JButton 	tbLastButton;
	private JButton 	tbNumberButton;
  	private JButton 	tbPauseButton;
  	private JButton 	tbPlayButton;
  	
	private String windowTitle                	= "Quotes";

	private String fileMenuText               	= "File";			//TODO: Add accelerator and shortcut key constants out here?
	private String fileNewMenuText            	= "New";
	private String fileOpenMenuText           	= "Open...";
	private String fileSaveMenuText           	= "Save";
	private String fileSaveAsMenuText         	= "Save as...";
	private String fileQuitMenuText           	= "Exit";

	private String editMenuText               	= "Edit";
	private String editUndoMenuText			  	= "Undo";
	private String editCutMenuText            	= "Cut";
	private String editCopyMenuText           	= "Copy";
	private String editPasteMenuText          	= "Paste";
	private String editClearMenuText          	= "Clear";
	private String editSelectAllMenuText      	= "Select all";

	private String quoteMenuText              	= "Quote";
	private String quoteFindMenuText          	= "Find...";
	private String quoteFindNextMenuText      	= "Find next";
	private String quoteFirstQuoteMenuText    	= "First quote";
	private String quotePrevQuoteMenuText     	= "Previous quote";
	private String quoteNextQuoteMenuText     	= "Next quote";
	private String quoteLastQuoteMenuText     	= "Last quote";
    private String quoteNumberedQuoteMenuText 	= "Numbered quote";
	private String quoteRandomQuoteMenuText   	= "Random quote";

	
	
	private String optionsMenuText			  	= "Options";
	private String optionsPreferencesMenuText 	= "Preferences...";

	private String 			helpMenuText		= "Help";
	private String 			helpAboutMenuText	= "About " + windowTitle + "...";
		
	private Container 		contentArea;
	private BorderLayout 	layout;
	
	//private JTextArea 	quoteText;
	//private JTextField 	quoteText;
	private JTextPane 		quoteText;
	private JTextField 		attributionText;
	
	private Font 			attributionFont;

	private static String 	filespec = "Default.quotes";		//args[1]
	private List<Quotation> quotes;	 //TODO: Create a Quotes object that's a list/collection of Quotation objects?	

	private Integer	  		currentQuoteNumber;
	
	private String 			textToFind = "quot";
	private int	  			quoteFound = 0;
	
	private Timer 			timer;

	
	//TODO: Add help dialog with list of keyboard shortcuts that aren't shown on menus
	// e.g. arrows, PgUp, PgDn, home, end, return, spacebar, etc etc.
	// (once those have been implemented.)

	private static int 	menuShortcutKeyMask = 0;


  	public static void main(String[] args)
  	{
  	  	// needed on Mac OS X to put menus in screen menu bar instead of window menu bar
		System.setProperty("apple.laf.useScreenMenuBar", "true");

  	  	// the proper way to show a jframe (invokeLater)
  	  	SwingUtilities.invokeLater(new QuotesApp());
  	  	
  	  	if ( args.length > 0 ) {
  	  		filespec = args[0];
  	  		System.out.println("Number of arguments:" + args.length);
  	  		System.out.println("args[0]=" + args[0]);
  	  		System.out.println("Filespec: " + filespec);
  	  	}

  	}

	public void run()
	{
		// Move all this GUI creation code to a separate method(s).  
		
		// get menu shortcut keyboard mask; Windows uses Event.CTRL_MASK, Mac uses Event.META_MASK (a.k.a. Command/Apple key)
		menuShortcutKeyMask = Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();

		//UNcomment the following to get Java look and feel instead of native L 'n F
		//JFrame.setDefaultLookAndFeelDecorated(true);

		frame   = new JFrame(windowTitle);
		menuBar = new JMenuBar();

		contentArea = frame.getContentPane();
		layout = new BorderLayout();
		contentArea.setLayout(layout);

		
		//TODO: Refactor menu building into separate methods for each menu.
		//TODO: Move menu-item related variables into the refactored methods.
		//TODO: Get rid of as many menu-related variables as possibles; at least those not needed after menu creation.

		// build the File menu
		fileMenu = new JMenu(fileMenuText);
		fileMenu.setMnemonic(KeyEvent.VK_F);

		fileNewMenuItem = new JMenuItem(fileNewMenuText, 'N');
		fileNewMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, menuShortcutKeyMask));
		fileNewMenuItem.addActionListener(this);
		fileMenu.add(fileNewMenuItem);

		fileOpenMenuItem = new JMenuItem(fileOpenMenuText,'O');
		fileOpenMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, menuShortcutKeyMask));
		fileOpenMenuItem.addActionListener(this);
		fileMenu.add(fileOpenMenuItem);

		fileMenu.add(new JSeparator());

		fileSaveMenuItem = new JMenuItem(fileSaveMenuText,'S');
		fileSaveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, menuShortcutKeyMask));
		fileSaveMenuItem.addActionListener(this);
		fileMenu.add(fileSaveMenuItem);

		fileSaveAsMenuItem = new JMenuItem(fileSaveAsMenuText,'A');
		fileSaveAsMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, menuShortcutKeyMask+ActionEvent.SHIFT_MASK));
		fileSaveAsMenuItem.addActionListener(this);
		fileMenu.add(fileSaveAsMenuItem);

		fileMenu.add(new JSeparator());

		//TODO: Add an EXIT menu too, or make ALT+F+X work somehow. Better yet, sense Mac and make it Quit/Cmd+Q, and Windows Exit/Ctrl+X & ALT+F+X
		//TODO: Hide "Exit" menu item on Mac.
		fileQuitMenuItem = new JMenuItem(fileQuitMenuText,'X');
		fileQuitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, menuShortcutKeyMask));
		fileQuitMenuItem.addActionListener(this);
		fileMenu.add(fileQuitMenuItem);

		menuBar.add(fileMenu);

		// build the Edit menu

		editMenu = new JMenu(editMenuText);
		editMenu.setMnemonic(KeyEvent.VK_E);
		
		editUndoMenuItem = new JMenuItem(editUndoMenuText,'U');
		editUndoMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, menuShortcutKeyMask));
		editUndoMenuItem.addActionListener(this);
		editMenu.add(editUndoMenuItem);

		editMenu.add(new JSeparator());

		editCutMenuItem = new JMenuItem(editCutMenuText,'T');
		editCutMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, menuShortcutKeyMask));
		editCutMenuItem.addActionListener(this);
		editMenu.add(editCutMenuItem);

		editCopyMenuItem = new JMenuItem(editCopyMenuText,'C');
		editCopyMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, menuShortcutKeyMask));
		editCopyMenuItem.addActionListener(this);
		editMenu.add(editCopyMenuItem);

		editPasteMenuItem = new JMenuItem(editPasteMenuText,'P');
		editPasteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, menuShortcutKeyMask));
		editPasteMenuItem.addActionListener(this);
		editMenu.add(editPasteMenuItem);

		editClearMenuItem = new JMenuItem(editClearMenuText,'L');
		editClearMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE,0));
		editClearMenuItem.addActionListener(this);
		editMenu.add(editClearMenuItem);

		editMenu.add(new JSeparator());

		editSelectAllMenuItem = new JMenuItem(editSelectAllMenuText,'A');
		editSelectAllMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, menuShortcutKeyMask));
		editSelectAllMenuItem.addActionListener(this);
		editMenu.add(editSelectAllMenuItem);

		menuBar.add(editMenu);

		//build the Quote menu

		quoteMenu = new JMenu(quoteMenuText);
		quoteMenu.setMnemonic(KeyEvent.VK_Q);

		quoteFindMenuItem = new JMenuItem(quoteFindMenuText,'F');
		quoteFindMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, menuShortcutKeyMask));
		quoteFindMenuItem.addActionListener(this);
		quoteMenu.add(quoteFindMenuItem);

		quoteFindNextMenuItem = new JMenuItem(quoteFindNextMenuText,'N');
		quoteFindNextMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, menuShortcutKeyMask));
		quoteFindNextMenuItem.addActionListener(this);
		quoteMenu.add(quoteFindNextMenuItem);

		quoteMenu.add(new JSeparator());

		quoteFirstQuoteMenuItem = new JMenuItem(quoteFirstQuoteMenuText, 'F');
		quoteFirstQuoteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, menuShortcutKeyMask));	// can't use H, CTRL-H is backspace.
		quoteFirstQuoteMenuItem.addActionListener(this);
		quoteMenu.add(quoteFirstQuoteMenuItem);

		quotePrevQuoteMenuItem = new JMenuItem(quotePrevQuoteMenuText, 'P');
		quotePrevQuoteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_J, menuShortcutKeyMask));
		quotePrevQuoteMenuItem.addActionListener(this);
		quoteMenu.add(quotePrevQuoteMenuItem);

		quoteNextQuoteMenuItem = new JMenuItem(quoteNextQuoteMenuText, 'N');
		quoteNextQuoteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, menuShortcutKeyMask));
		quoteNextQuoteMenuItem.addActionListener(this);
		quoteMenu.add(quoteNextQuoteMenuItem);

		quoteLastQuoteMenuItem = new JMenuItem(quoteLastQuoteMenuText, 'L');
		quoteLastQuoteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, menuShortcutKeyMask));
		quoteLastQuoteMenuItem.addActionListener(this);
		quoteMenu.add(quoteLastQuoteMenuItem);

		quoteMenu.add(new JSeparator());

		quoteNumberedQuoteMenuItem = new JMenuItem(quoteNumberedQuoteMenuText,'U');
		quoteNumberedQuoteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, menuShortcutKeyMask));
		quoteNumberedQuoteMenuItem.addActionListener(this);
		quoteMenu.add(quoteNumberedQuoteMenuItem);

		quoteRandomQuoteMenuItem = new JMenuItem(quoteRandomQuoteMenuText,'R');
		quoteRandomQuoteMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, menuShortcutKeyMask));
		quoteRandomQuoteMenuItem.addActionListener(this);
		quoteMenu.add(quoteRandomQuoteMenuItem);

		menuBar.add(quoteMenu);

		// build the Options menu
		optionsMenu = new JMenu(optionsMenuText);
		optionsMenu.setMnemonic(KeyEvent.VK_O);

		optionsPreferencesMenuItem = new JMenuItem(optionsPreferencesMenuText,'P');		//TODO Make OS specific: Preferences/Options, accelerator key
		optionsPreferencesMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_COMMA, menuShortcutKeyMask));
		optionsPreferencesMenuItem.addActionListener(this);
		optionsMenu.add(optionsPreferencesMenuItem);

		menuBar.add(optionsMenu);


		// build the Help menu
		helpMenu = new JMenu(helpMenuText);
		helpMenu.setMnemonic(KeyEvent.VK_H);

		helpAboutMenuItem = new JMenuItem(helpAboutMenuText,'A');
		helpAboutMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		helpAboutMenuItem.addActionListener(this);
		helpMenu.add(helpAboutMenuItem);

		menuBar.add(helpMenu);

		// hook menubar up to the Frame
		frame.setJMenuBar(menuBar);		
 
								
		//TODO Add toolbar with first, prev, next, last, play/pause (random) cut, copy, paste, help, prefs buttons

		// Toolbar setup
		
		toolbar = new JToolBar ("Quote tools");
		
		tbRandomButton = new JButton(quoteRandomQuoteMenuText);
		tbRandomButton.addActionListener(this);
		tbRandomButton.setActionCommand(quoteRandomQuoteMenuText);
		tbRandomButton.setToolTipText("Display a new random quote from the quote database");
		toolbar.add(tbRandomButton);
		
		tbFirstButton = new JButton("|<");
		tbFirstButton.addActionListener(this);
		tbFirstButton.setActionCommand(quoteFirstQuoteMenuText);
		tbFirstButton.setToolTipText(quoteFirstQuoteMenuText);
		toolbar.add(tbFirstButton);
		
		tbPrevButton = new JButton("< ");
		tbPrevButton.addActionListener(this);
		tbPrevButton.setActionCommand(quotePrevQuoteMenuText);
		tbPrevButton.setToolTipText(quotePrevQuoteMenuText);
		toolbar.add(tbPrevButton);
		
		tbNextButton = new JButton(" >");
		tbNextButton.addActionListener(this);
		tbNextButton.setActionCommand(quoteNextQuoteMenuText);
		tbNextButton.setToolTipText(quoteNextQuoteMenuText);
		toolbar.add(tbNextButton);
		
		tbLastButton = new JButton(">|");
		tbLastButton.addActionListener(this);
		tbLastButton.setActionCommand(quoteLastQuoteMenuText);
		tbLastButton.setToolTipText(quoteLastQuoteMenuText);
		toolbar.add(tbLastButton);

		toolbar.addSeparator();

		tbNumberButton = new JButton("#");			
		tbNumberButton.addActionListener(this);
		tbNumberButton.setActionCommand(quoteNumberedQuoteMenuText);
		tbNumberButton.setToolTipText(quoteNumberedQuoteMenuText);
		toolbar.add(tbNumberButton);

		tbPauseButton = new JButton("PAUSE");			//TODO Implement play/pause to flip randomly (or sequentially?) through quotes
		tbPauseButton.addActionListener(this);
		tbPauseButton.setActionCommand("PAUSE");
		tbPauseButton.setEnabled(false);
		toolbar.add(tbPauseButton);

		tbPlayButton = new JButton("PLAY");
		tbPlayButton.addActionListener(this);
		tbPlayButton.setActionCommand("PLAY");
		tbPlayButton.setEnabled(false);
		toolbar.add(tbPlayButton);

		toolbar.addSeparator();

//		JTextField tbNumberField = new JTextField("123456");
//		tbNumberField.setToolTipText("Enter quote number to display here.");
//		toolbar.add( tbNumberField );
 
		contentArea.add(toolbar, BorderLayout.NORTH);
		
		// Text areas

		// Set up quote display text area and attribution display textarea

		//quoteText = new JTextField(); 
		//quoteText = new JTextArea();
		quoteText = new JTextPane();
		//quoteText.setLineWrap( true );
		//quoteText.setWrapStyleWord(true);
		//quoteText.setText( "The mind is everything. \r\nWhat you think, you become." );
		quoteText.setEditable(false);
		quoteText.setMargin(new Insets(25, 25, 25, 25));   // top, left, bottom, right

		//TODO Make fonts, size, style, color etc settable from a properties files, eventually a properties dialog.
		//TODO Make use of random fonts, style, color possible too (one setting on the popups for font, etc.
		Font quoteFont = new Font("Papyrus", Font.BOLD, 36);	//"Comic Sans MS", "Lucida Calligraphy" "Lucida Handwriting"
		quoteText.setFont(quoteFont);
		//quoteText.setAlignmentX(java.awt.Component.CENTER_ALIGNMENT) ;
		//quoteText.setAlignmentY(java.awt.Component.CENTER_ALIGNMENT);
		
		contentArea.add( quoteText, BorderLayout.CENTER );
	       
        try {

            quoteText.setEditorKit( new MyEditorKit() );
            SimpleAttributeSet attrs = new SimpleAttributeSet();
            StyleConstants.setAlignment(attrs,StyleConstants.ALIGN_CENTER);
            
            StyledDocument doc = (StyledDocument)quoteText.getDocument();
            doc.insertString(0,"The mind is everything. \r\nWhat you think, you become.",attrs);
            doc.setParagraphAttributes(0,doc.getLength()-1,attrs,false);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

		// Set up the attribution text area (who said the quote)

		attributionText = new JTextField();
		attributionText.setText(" - Buddha");
		//attributionText.setLineWrap(false);
		attributionText.setBackground(Color.lightGray);
		attributionText.setHorizontalAlignment(JTextField.RIGHT);
		
		//TODO Change font size based on length of string, so that text might fit better.  Better yet, use bounds of window to calculate a font size that will fit.
		attributionFont = new Font("Arial Black",Font.ITALIC+Font.BOLD,16); 
		attributionText.setFont(attributionFont);
	
		contentArea.add(attributionText, BorderLayout.SOUTH);

		//TODO Make left/right BorderLayout areas into next/prev buttons?

		//JButton prevQuoteButton = new JButton("<");
		//contentArea.add(prevQuoteButton, BorderLayout.WEST);

		//JButton nextQuoteButton = new JButton(">");
		//contentArea.add(nextQuoteButton, BorderLayout.EAST);

		
		// Finish up frame setup and show it.

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(800, 600)); // width, height 
		frame.pack();

		//TODO This is NOT working (or perhaps getImage() is not)
		// set up window icon (upper left corner)

		Image icon20x20 = getImage("./icons/Quotes16x16.jpg");
		Image icon26x26 = getImage("./icons/Quotes16x16.jpg");
		Image icon28x28 = getImage("./icons/Quotes16x16.jpg");

		setFrameIconImage( frame, icon20x20, icon26x26, icon28x28 );  // must be AFTER frame.pack() or .show()

		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		frame.setContentPane(contentArea);

		// set up timer to display new random quote every so often
		
		int   pause = 1000 * 15;  // time in milliseconds, 1000 milliseconds=1 second
		timer = new Timer(pause, new ActionListener() {
		    public void actionPerformed(ActionEvent evt) {
		    	//System.out.println("Timer activated");
		    	displayRandomQuote();		    	
		        }    
		    });
	    timer.setInitialDelay(pause);
	    timer.start(); 

	    
		//Open a quote file (Default.quotes in local dir by default) and display a random quote
	    //TODO: Figure out support for Default.quotes being built into .jar and .app bundles
	    //TODO: Add support for .quotes (.qts?) file type (e.g. "Quotes.quotes" or "ScienceFiction.quotes" or "Heinlein.quotes" 
	    //TODO: Add support for drag-and-drop onto app icon or text window of .quotes or .txt files.
	    //TODO: Eventually add SQL database support esp. SqLite3. Build SqLite 3 into app/jar bundle?
	    //TODO: Add support for File>New, File>Open, File>Save File>Save As; File>Close; close by goes back to default file 
	    //TODO: Add support for cut, paste, copy, edit of quote in memory and saving file back to disk.
	   		
		quotes = QuoteFileReader.readQuotesFromFile(filespec);
		System.out.println("Read " + quotes.size() + " quotes from file " + filespec);
		displayRandomQuote();

		
		//TODO: How do we handle other keystrokes besides accelerator/shortcuts?
		// Also mouse clicks? 
		//frame.addKeyListener();	// arrow keys, home, end, pgup/pgdn, space, esc=exit?
		//frame.addMouseListener();  or perhaps
		//quoteText.frame.addMouseListener()
		//frame.addMouseWheelListener(); // to allow mouse wheel to scroll up/down thru.
		//TODO: Clicking on attribution field does Find for that person and shows first quote.  Eventually displays list of all quotes?  Or click shows first, dbl-click shows list?
	}


 	public void actionPerformed(ActionEvent ev)
  	{
		// Handle File menu items
 		
		if (ev.getActionCommand() == fileNewMenuText) 			msgBox("New command not yet implemented.");			//TODO: what do to with File>New? If we add ability to insert a quote, then it will
		if (ev.getActionCommand() == fileOpenMenuText) 			msgBox("Open command not yet implemented.");		//TODO: Let user select quote file other than default. Clear ArrayList quotes and read it in, show random quote or first)
		if (ev.getActionCommand() == fileSaveMenuText) 			msgBox("Save command not yet implemented.");		//TODO: Save current quote ArrayList back to current file (need currentFile var)
		if (ev.getActionCommand() == fileSaveAsMenuText) 		msgBox("Save as command not yet implemented.");		//TODO: Saves current quote ArrayList to a new file
		if (ev.getActionCommand() == fileQuitMenuText)
		{
			System.exit(0);
			//TODO: Eventually allow saving of arraylist of quotes if it's been modified
		}

		
		//Handle Edit menu items

		//TODO: Eventually let people cut/copy/paste quotes
		//TODO: Need a quote text editor window, to let user edit text of current quote. will need different actionlisteners for that window
		
		if (ev.getActionCommand() == editUndoMenuText) 			msgBox("Undo command not yet implemented.");		//TODO: This will take some though. Do we need Redo too?
		if (ev.getActionCommand() == editCutMenuText) 			msgBox("Cut command not yet implemented.");			//TODO: Make this cut quote from file. Use quotes.remote(currentQuote)
		if (ev.getActionCommand() == editCopyMenuText) 			msgBox("Copy command not yet implemented.");		//TODO: Copy this to clipboard.
		if (ev.getActionCommand() == editPasteMenuText)			msgBox("Paste command not yet implemented.");		//TODO: Prompt user to add contents of clipboard as a quote. Use quotes.add(quotes.size()) ? 

//			msgBox("Clipboard contents:\n"+ getClipboardContents());

		if (ev.getActionCommand() == editClearMenuText) 		msgBox("Clear command not yet implemented.");		//TODO: delete the current quote (quotes.remote(currentQuote))
		if (ev.getActionCommand() == editSelectAllMenuText) 	msgBox("Select all command not yet implemented.");	//TODO: May not be applicable since cut/copy/paste refer to quotes not text
		

		//Handle Quote menu items

		if (ev.getActionCommand() == quoteFindMenuText) 		findQuote();
		if (ev.getActionCommand() == quoteFindNextMenuText) 	findNextQuote(quoteFound, textToFind);
		
		if (ev.getActionCommand() == quoteFirstQuoteMenuText) 	displayFirstQuote();
		if (ev.getActionCommand() == quotePrevQuoteMenuText) 	displayPreviousQuote();
		if (ev.getActionCommand() == quoteNextQuoteMenuText) 	displayNextQuote();
		if (ev.getActionCommand() == quoteLastQuoteMenuText) 	displayLastQuote();
		
		if (ev.getActionCommand() == quoteNumberedQuoteMenuText) displayNumberedQuote();	//TODO Added dialog box to ask for quote #, validate min/max
		if (ev.getActionCommand() == quoteRandomQuoteMenuText)	 displayRandomQuote() ;


		//Handle Options menu items

		if (ev.getActionCommand() == optionsPreferencesMenuText) msgBox("Preferences command not yet implemented.");	//TODO implement properties file and preferences dialog, save/set props.


		//Handle Help menu items

		if (ev.getActionCommand() == helpAboutMenuText) {
			displayHelpAboutDialog();
		}
		
  	}

 	public String findDialog( String initialValue ) 
 	{
 	  	//showInputDialog(Component parentComponent, Object message, String title, int messageType, Icon icon, Object[] selectionValues, Object initialSelectionValue) 
 		//String response = JOptionPane.showInputDialog( msg, initialValue );
 		String response = (String) JOptionPane.showInputDialog( frame, "Enter text to search for in quotes", "Find", JOptionPane.QUESTION_MESSAGE, null,  null, initialValue  );
 	 	return response;
 	}

 	public void findQuote() 
 	{
 		textToFind = findDialog(textToFind);
 		System.out.println("inputBox() returned '" + "'");
 		if ( !textToFind.equals(null) && !textToFind.equals("") ) {
 			quoteFound = -1;
 			findNextQuote(quoteFound, textToFind );
 		}
 	}

 	public void findNextQuote(int lastQuote, String toFind ) 
 	{	
 		int savedLastQuote = lastQuote; 
 		int newStart = lastQuote + 1;
 		System.out.println("Starting search at quote #" + newStart + " for text '" + toFind + "'");
 		int q ;
 		
 		for (q= newStart ; q < quotes.size(); q++) {
			String quote = quotes.get(q).getText().toLowerCase();
			String attribution = quotes.get(q).getAttribution().toLowerCase();
			if ( ( quote.contains( toFind.toLowerCase() ) )
			||   ( attribution.contains( toFind.toLowerCase() ) )
			   ) {
				System.out.println("Found '" + toFind + "' at quote #" + q + ": \n" + quotes.get(q).getText() + "\n  - " + quotes.get(q).getAttribution() );
 				displayQuote( getNumberedQuote(q) );
				quoteFound = q;
				break;
			}
		} 		
 		if ( q >= quotes.size() ) {
 	 		// if we get here we did not find another matching quote;
	 		System.out.println("No matching quote found, returning # of last quote found:" + savedLastQuote);
	 		msgBox("No more quotes found matching '"+ toFind + "'");
			quoteFound = savedLastQuote;
 		}
 	}

 	public void displayNumberedQuote() 
 	{
 		String qnum = inputBox("Quote Number to display (1-" + quotes.size()+"):", Integer.toString(currentQuoteNumber+1) );
 		displayQuote(getNumberedQuote(Integer.valueOf(qnum)-1));
 	}

 	public void displayRandomQuote() 
 	{
 		Quotation quote = getRandomQuote();
 		displayQuote(quote);
 	}

 	public void displayFirstQuote() 
 	{
 		Quotation quote = getNumberedQuote(0);						//TODO replace this with a global "currentQuote" and also a currentQuoteNumber int.
 		displayQuote(quote);
 	}

 	public void displayPreviousQuote() 
 	{
 		currentQuoteNumber = currentQuoteNumber - 1;
 		System.out.println("Quote number #"+currentQuoteNumber + " of " + quotes.size());
 		
 		if (currentQuoteNumber <= 0) {
 			currentQuoteNumber = quotes.size()-1 ;  // wrap around to end
 			System.out.println("Quote number <= 0; wrapping.  New Quote number #"+currentQuoteNumber + " of " + quotes.size());
 		}
 		Quotation quote = getNumberedQuote(currentQuoteNumber);		//TODO replace this with a global "currenQuote" and also a currentQuoteNumber int.
		displayQuote(quote);
 	}

 	public void displayNextQuote() 
 	{
 		currentQuoteNumber = currentQuoteNumber+1;
 		if (currentQuoteNumber > (quotes.size()-1) ) {
 			currentQuoteNumber = 0 ;  // wrap around to first quote
 		}
 		Quotation quote = getNumberedQuote(currentQuoteNumber);		//TODO replace this with a global "currenQuote" and also a currentQuoteNumber int.
		displayQuote(quote);
 	}
 	
 	public void displayLastQuote() 
 	{
 		Quotation quote = getNumberedQuote(quotes.size()-1);			//TODO may need to be .size()-1
 		displayQuote(quote);
 		//TODO Set quote # in window frame title. 
 	}
 	
 	public void displayQuote(Quotation quote) 
 	{
		quoteText.setText(quote.getText());
		attributionText.setText(quote.getAttribution()); 	 		
 		frame.setTitle("Quotation #" + (currentQuoteNumber+1) + " of " + quotes.size() );
 		timer.restart();
 	}
 	
 	public Quotation getRandomQuote() 
 	{
 		Quotation quote = new Quotation();							//TODO change to use currentQuote
		Random r = new Random();
		quote = getNumberedQuote( r.nextInt(quotes.size()-1 ) );
 		return quote;
 	}

 	//NOTE:  Always use this to get a numbered quote; don't do it elsewhere or you'll break currenQuote and currentQuoteNumber;
 	//TODO: This needs to be turned into a method of some sort of class
 	
 	public Quotation getNumberedQuote(Integer n) 
 	{
 		Quotation quote = new Quotation();
 		if (quotes.size() > 0 ) {
	 		if ( n <= (quotes.size()-1) ) {			 
	 			quote = quotes.get(n);			//TODO change to use currentQuote
	 			currentQuoteNumber = n;
	 		} else {
	 			quote.setText("Sorry, no quote #" + n + " in file!");
	 			quote.setAttribution("The Programmer");
	 		}
 		} else { 
 			quote.setText("Sorry, no quotes in file!");
 			quote.setAttribution("The Programmer"); 			
 		}
 		
 		// Replace \ in quote.text with \n
 		quote.text = quote.text.replaceAll("\\|","\n");
 		
 		return quote;
 	}
 	
 	
 	
	public void displayHelpAboutDialog()
	{
	    AboutDialog aboutDialog = new AboutDialog();
	    aboutDialog.setModal(true);
	    aboutDialog.setVisible(true);
	}

  	/**
  	 * This dialog is displayed when the user selects the Help/About menu item.
  	 */

  	private class AboutDialog extends JDialog implements ActionListener
  	{
  		private static final long serialVersionUID = 1L;		// class version # for serialization; change this if class changed to be non-backwards compatible?
  		
    	private JButton okButton = new JButton("OK");
		private JTextArea  aboutText = new JTextArea(5,25);

    	private AboutDialog()
    	{
    	  super(frame, "About " + windowTitle, true);
    	  JPanel panel = new JPanel(new BorderLayout());
    	  
    	  aboutText.setText( windowTitle + "\r\nCopyright © 2010-2017 William B. Davis, Jr.\r\nAll Rights Reserved." );
		  
    	  Font aboutTextFont = new Font("Comic Sans MS", Font.BOLD, 24);
		  aboutText.setFont(aboutTextFont);
		  
		  aboutText.setLineWrap(true);
		  aboutText.setWrapStyleWord(true);
		  aboutText.setEditable(false);
		  aboutText.setMargin(new Insets(5,5,5,5));
		  panel.add(aboutText, BorderLayout.CENTER);
    	  
		  panel.add(okButton, BorderLayout.SOUTH);
    	  getContentPane().add(panel);
    	  
    	  okButton.addActionListener(this);
    	  setPreferredSize(new Dimension(300, 200));
    	  pack();
    	  setLocationRelativeTo(frame);
			
    	  //TODO: Put info such as Java version running, etc in here, or perhaps add a button to bring up another dialog with
			// support info.
    	}

    	public void actionPerformed(ActionEvent ev)
    	{
			if (ev.getActionCommand() == "OK" ) setVisible(false);

    	}
	}

  	// Generic text-input dialog box
  	//TODO Modify to use question-mark icon and to allow us to set values of buttons, e.g. Find/Cancel etc.
  	//showInputDialog(Component parentComponent, Object message, String title, int messageType, Icon icon, Object[] selectionValues, Object initialSelectionValue) 
  	
  	public String inputBox(String msg, String initialValue) {
  		String response = JOptionPane.showInputDialog( msg, initialValue );
 		return response;
  	}
  	
  	
	// Generic message dialog for easy display of a message with an OK button

	public void msgBox(String msg)
	{
		MsgBox msgbox = new MsgBox( msg );
		msgbox.setModal( true );
		msgbox.setVisible( true );
	}

  	private class MsgBox extends JDialog implements ActionListener
  	{
  		private static final long serialVersionUID = 1L;		// class version # for serialization; change this if class changed to be non-backwards compatible?

  		private JButton okButton 	= new JButton("OK");
		private JTextArea  msgText 	= new JTextArea(5,25);

    	private MsgBox(String msg)
    	{
    	  super(frame, "Message from " + windowTitle, true);		//TODO: Change to not use windowTitle, but instead app name?
    	  JPanel panel = new JPanel(new BorderLayout());

		  msgText.setText( msg );

		  Font msgTextFont = new Font("Arial Bold", Font.BOLD, 16);
		  msgText.setFont(msgTextFont);
		  
		  msgText.setLineWrap(true);
		  msgText.setWrapStyleWord(true);
		  msgText.setEditable(false);  // can still copy from it, just can't edit.
		  msgText.setMargin(new Insets(5,5,5,5));
		  
		  panel.add( msgText, BorderLayout.CENTER );

    	  panel.add( okButton, BorderLayout.SOUTH );
    	  getContentPane().add(panel);
    	  okButton.addActionListener(this);
    	  setPreferredSize(new Dimension(300, 200));
    	  pack();
    	  setLocationRelativeTo(frame);
       	}

    	public void actionPerformed(ActionEvent ev)
    	{
			if (ev.getActionCommand() == "OK") setVisible(false);
    	}
	}

 	/** This method sets the icon image of the frame according to the best imageIcon size requirements for the system's appearance settings.
	  * IMPORTANT NOTE: This method should only be called after pack() or show() has been called for the Frame.
	  * @param frame The Frame to set the image icon for.
	  * @param image20x20 An image, 20 pixels wide by 20 pixels high
	  * @param image26x26 An image, 26 pixels wide by 26 pixels high
	  * @param image28x28 An image, 28 pixels wide by 28 pixels high
	  */
	private void setFrameIconImage(Frame frame, Image image20x20, Image image26x26, Image image28x28)
	{
		Insets insets = frame.getInsets();

		int titleBarHeight = insets.top;

		if (titleBarHeight == 32)
		{
		 //It's 'Windows Classic Style with Large Fonts', so use a 26 x 26 image
		 if (image26x26 != null) frame.setIconImage(image26x26);
		}
		else
		{
		 //Use the default 20 x 20 image - Looks fine on all other Windows Styles & Font Sizes
		 //(except 'Windows Classic Style with Extra Large Fonts' where image is slightly distorted.
		 //Have to live with that as cannot differentiate between 'Windows XP Style with Normal Fonts'
		 // appearance and 'Windows Classic Style with Extra Large Fonts' appearance as they both have the same insets values)
		 if (image20x20 != null) frame.setIconImage(image20x20);
		}
	}

    //Returns an Image or null.
    //TODO: How to mask the image to let the background through the "clear" spots?
    protected static Image getImage(String filespec)
    {
        URL imgURL = QuotesApp.class.getResource(filespec);

        if (imgURL != null)
        {
            return new ImageIcon(imgURL).getImage();
        }
        else
        {
            return null;
        }
    } // getImage()


    /**
	  * Get string from the system clipboard
	  */

 	public static String getClipboardContents()
   	{
		String result = "";

		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

		//odd: the Object param of getContents is not currently used
		Transferable contents = clipboard.getContents(null);

		boolean hasTransferableText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);

		if ( hasTransferableText ) {
		  try
		  {
			result = (String)contents.getTransferData(DataFlavor.stringFlavor);
		  }
		  catch (UnsupportedFlavorException ex)
		  {
			//highly unlikely since we are using a standard DataFlavor
			System.out.println(ex);
			ex.printStackTrace();
		  }
		  catch (IOException ex)
		  {
			System.out.println(ex);
			ex.printStackTrace();
		  }
		}
		return result;
  	}


 	
} // class Quotes

class MyEditorKit extends StyledEditorKit {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ViewFactory getViewFactory() {
        return new StyledViewFactory();
    }
 
    static class StyledViewFactory implements ViewFactory {

        public View create(Element elem) {
            String kind = elem.getName();
            if (kind != null) {
                if (kind.equals(AbstractDocument.ContentElementName)) {

                    return new LabelView(elem);
                } else if (kind.equals(AbstractDocument.ParagraphElementName)) {
                    return new ParagraphView(elem);
                } else if (kind.equals(AbstractDocument.SectionElementName)) {

                    return new CenteredBoxView(elem, View.Y_AXIS);
                } else if (kind.equals(StyleConstants.ComponentElementName)) {
                    return new ComponentView(elem);
                } else if (kind.equals(StyleConstants.IconElementName)) {

                    return new IconView(elem);
                }
            }
 
            return new LabelView(elem);
        }

    }
}
 
class CenteredBoxView extends BoxView {
	
    public CenteredBoxView(Element elem, int axis) {

        super(elem,axis);
    }
    protected void layoutMajorAxis(int targetSpan, int axis, int[] offsets, int[] spans) {

        super.layoutMajorAxis(targetSpan,axis,offsets,spans);
        int textBlockHeight = 0;
        int offset = 0;
 
        for (int i = 0; i < spans.length; i++) {

            textBlockHeight = spans[i];
        }
        offset = (targetSpan - textBlockHeight) / 2;
        for (int i = 0; i < offsets.length; i++) {
            offsets[i] += offset;
        }

    }
}    

