import java.io.*;

import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.Map;

//import javax.xml.XMLConstants;
//import javax.xml.transform.Source;
//import javax.xml.transform.stream.StreamSource;
//import javax.xml.validation.Schema;
//import javax.xml.validation.SchemaFactory;
//import javax.xml.validation.Validator;
//
//import org.xml.sax.SAXException;

import com.thoughtworks.xstream.XStream;

//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.apache.log4j.Level;

//
// Takes a file of the form:
//    P:person's name
//    Q:quotation by that person
//    Q:another quotations by that person
//    ...
//    Q:last quotation by that person;
//	  P:another person's name
//    Q:quotation by that person
//    ...
//    Q:last quotation by that person;
//
// and outputs it as
//
//     quotation by that person
//       - person's name
//
//     another quotation by that person
//       - person's name
//
//     etc.


public class Flat2XML
{
	public static void main (String[] args)
	{

	  //Quotes 				quotes 		= new Quotes("urn:billdavis.home.mchsi.com:quote-io");
		Quotes 				quotes 		= new Quotes();

		ArrayList<Person> 	list 		= new ArrayList<Person>();
		ArrayList<Quote> 	quoteList 	= new ArrayList<Quote>();

		long lineCount          = 0;
		long personCount        = 0;
		long quoteCount         = 0;
		long quoteByPersonCount = 0;

		try
		{

			String filename = "quotes.txt";

			// if first command line arg is not missing, use it as filename/filespec
			// instead of default 'quotes.txt'

			if (args.length > 0)
			{
				filename = args[0];
			}

			FileReader 		file 	= new FileReader(filename);
			BufferedReader 	buffer 	= new BufferedReader(file);


			// Set default values for person and quotation until filled from data in file.
			String person 		= "Unknown";
			String quotation 	= "The quick brown fox jumped over the lazy dog.";

			String textline 	= null;
			Person p 			= null;


			while ((textline = buffer.readLine()) != null)
			{
				lineCount++;

				//System.out.println(textline);

				char sw = textline.toUpperCase().charAt(0);		// should be P or Q
				//char colon = textline.toUpperCase().charAt(1);	// should be ':'
				//TODO: Make sure 2nd char is a colon, skip line if not.  Then check for P or Q.

				switch (sw) {
					case 'P':
					{
						break;
					}

					case 'Q':
					{
						break;
					}
					default:
					{
						System.out.println("Unknown code on line " + lineCount +": '" + textline + "'");
						System.out.println();

						break;
					}
				}


				if (textline.startsWith("P:"))
				{

					if (p != null) {
						System.out.println("Quotes by " + person + ":" + quoteByPersonCount);
						System.out.println();
						System.out.println();
					}

					person = textline.substring(2);  // get entire line from 3rd character on (0th and 1st chars are "P:")

					if (p != null)
					{
						p.setQuoteRecs(quoteList);
						list.add(p);

						personCount++;
					}

					p = new Person();
					p.setName(person);

					quoteList = new ArrayList<Quote>();

					quoteByPersonCount = 0;


				}

				if (textline.startsWith("Q:"))
				{
					quotation = textline.substring(2);

					Quote q = new Quote();
					q.setQuotation(quotation);
					q.setPerson(person);

					quoteList.add(q);
					quoteCount++;
					quoteByPersonCount++;

					System.out.println("\""+q.getQuotation()+"\"");
					System.out.println("  - " + q.getPerson());
					System.out.println();
				}


			} // while

			//write final person's stuff to file when we reach EOF.
			//TODO: Make this a method to avoid code duplciation

			if (p != null)
			{
				p.setQuoteRecs(quoteList);
				list.add(p);
				personCount++;
				System.out.println("Quotes by " + person + ":" + quoteByPersonCount);
			}

			buffer.close();

			quotes.setPersonRecs(list);

			System.out.println();
			System.out.println("TOTALS:");
			System.out.println("Total persons: " + personCount);
			System.out.println("Total quotes : " + quoteCount);

			//TODO:  Output Quotes structure using for loops?

			//TODO: find code to open save dialog to get filespec to save XML file into,
			//and pass filespec into writeXMLfile(), default to "quotes.xml" if null.

			File xmlFile = writeXMLfile(quotes);

			//TODO: Create an XSD for the XML file we want to output.
			//TODO:  validate XML file against XSD

			// Open TextPad with XML file we just wrote.

			String[] command = new String[4];

			command[0] = "cmd";
			command[1] = "/C";
			command[2] = "textpad ";
			command[3] = xmlFile.getPath();

			Process process = Runtime.getRuntime().exec(command);
			try {
				int result = process.waitFor();
				System.out.println("Generated file "+ xmlFile.getPath() + " opened in TextPad: " + result);
			} catch (Exception e) {
				System.out.println("Error occurred opening document in TextPad:" + e.getMessage());
			}

		} //try

		catch (IOException e)
		{
			System.out.println(e);

		} //catch

	} //main()


	private static File writeXMLfile(Quotes q)
	{
		File xmlFile = null;

		try
		{
			XStream xstream = new XStream();

			xstream.alias("Quotes", Quotes.class);
			xstream.alias("Person", Person.class);
			xstream.alias("Quote",  Quote.class);

			xstream.addImplicitCollection(Quotes.class, "personRecs");
			xstream.addImplicitCollection(Person.class, "quoteRecs");

			//xstream.useAttributeFor(Quotes.class, "xmlns"); //todo: find out what this is


			//TODO: move write xml stream code to separate method.


			//xmlFile = File.createTempFile("./tempQuotes",".xml");  //TODO change to non temp file, remove code in finally?
			xmlFile = new File("./quotes.xml"); //TODO: Add filespec parameter to writeToXMLfile() method parameters.

			BufferedWriter outBW = new BufferedWriter(new FileWriter(xmlFile));

			outBW.write(xstream.toXML(q));
			outBW.close();

			System.out.println("Successfully wrote XML file " + xmlFile.getPath());

			//TODO: move validation code to separate method
			//TODO: move try/catch for SAXException around this block of code?

			//System.out.println("Validating XML file " + xmlFile.getPath() + " against XSD.");

			//String xsdFilespec = "./quotes.xsd";

			//SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			//StreamSource streamsource = new StreamSource(xsdFilespec);  //TODO: Move to properties
			//Schema schema = factory.newSchema(streamsource);
			//Validator validator = schema.newValidator();
			//Source source = new StreamSource(xmlFile);
			//validator.validate(source);

			//System.out.println("Successfully validated XML file " + xmlFile.getPath() + " against XSD file " + xsdFilespec);

		}

		catch (IOException e)
		{
			if (xmlFile != null)
			{
				System.out.println("Unexpected IO error writing XML file " + xmlFile.getPath() + " : ");
				System.out.println(e.getMessage());

			}
			else
			{
				System.out.println("Unexpected error writing XML file - file not yet created:");
				System.out.println(e.getMessage());
			}

			System.out.println("Stack trace follows:");
			e.printStackTrace();

		}

//		catch (SAXException se)
//		{
//			System.out.println("Exception while validating XML: " + se.getMessage());
//			se.printStackTrace();
//
//		}

		catch (Exception ex)
		{
			System.out.println("Unexpected error writing XML file: " + ex.getMessage());
			ex.printStackTrace();
		}

		finally
		{
			//if (xmlFile != null) xmlFile.deleteOnExit();  // tell Java VM to remove temp file on exit of program, if error
		}

		return xmlFile;

	} // writeXMLfile

} // class

//TODO: Now write the REVERSE of this, to create a flat file from the XML File this program creates.

//TODO: Go through Java in Easy Steps book and turn this into a web applet, and a stand-alone app.

//TODO: Add option to write quotes the way they are displayed and the way they are read by my VB6 app.

//TODO: Add option to read XML file into Quotes->Person->Quote structure and write to P:person/Q:quote.. text file

