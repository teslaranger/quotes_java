import java.util.ArrayList;

public class Person
{

	private String 		name = "";
	private ArrayList<Quote> 	quoteRecs = new ArrayList<Quote>();

	public Person()
	{

	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * @param person the person to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the quoteRecs
	 */
	public ArrayList<Quote> getQuoteRecs()
	{
		return quoteRecs;
	}

	/**
	 * @param quoteRecs the quoteRecs to set
	 */
	public void setQuoteRecs(ArrayList<Quote> quoteRecs)
	{
		this.quoteRecs = quoteRecs;
	}

}