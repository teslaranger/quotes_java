//import java.util.ArrayList;

public class Quote
{
	private String quotation 	= "";
	private String person 		= "Unknown";

	public Quote()
	{

	}

	/**
	 * @return the quotation
	 */
	public String getQuotation()
	{
		return this.quotation;
	}

	/**
	 * @param quotation the quotation to set
	 */
	public void setQuotation(String quotation)
	{
		this.quotation = quotation;
	}

	/**
	 * @return the person
	 */
	public String getPerson()
	{
		return this.person;
	}

	/**
	 * @param person the person to set
	 */
	public void setPerson(String person)
	{
		this.person = person;
	}

}