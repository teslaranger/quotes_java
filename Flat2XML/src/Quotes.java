import java.util.ArrayList;

public class Quotes
{

	private String xmlns = null;
	private ArrayList<Person> personRecs = new ArrayList<Person>();

	public Quotes()
	{

	}

	public Quotes(String val)
	{
		xmlns = val;
	}

	/**
	 * @return the xmlns
	 */
	public String getXmlns()
	{
		return xmlns;
	}

	/**
	 * @param xmlns the xmlns to set
	 */
	public void setXmlns(String xmlns)
	{
		this.xmlns = xmlns;
	}

	/**
	 * @return the personRecs
	 */
	public ArrayList<Person> getPersonRecs()
	{
		return personRecs;
	}

	/**
	 * @param personRecs the personRecs to set
	 */
	public void setPersonRecs(ArrayList<Person> personRecs)
	{
		this.personRecs = personRecs;
	}


}
