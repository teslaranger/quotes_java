package com.williambdavisjr.util;

public class Number {


	public static boolean isEven(int number) 
	{
		if ((number % 2) == 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isOdd(int number) 
	{
		if ((number % 2) != 0) {
			return true;
		} else {
			return false;
		}
	}
	
}
