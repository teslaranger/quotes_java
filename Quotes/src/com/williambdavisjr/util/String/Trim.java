package com.williambdavisjr.util.String;

public class Trim {
	
//	TODO: Make this do more than a character; do any length. Rename to trimLeadingAndTrailing()	
	public static String leadingAndTrailing(String toBeTrimmed, String charToTrim) {

		String trimmed = toBeTrimmed;

		trimmed = Trim.leading(trimmed, charToTrim);
		trimmed = Trim.trailing(trimmed, charToTrim);

		return trimmed;
	}

//	TODO: Make this do more than a character; do any length. Rename to trimLeadingString or trimLeading
//	TODO: Use regex and .replaceFirst() to do the trimming?
	public static String leading(String toBeTrimmed, String charToTrim) {

		String trimmed = toBeTrimmed;

		while ( (trimmed.length() > 0) && ( trimmed.substring(0,1).equals(charToTrim) ) ) {
			trimmed = trimmed.substring(1);
		}

		return trimmed;
	}
	
//	TODO: Make this do more than a character; do any length.  Rename to trimTrailingString or trimTrailing.
//	TODO: Use regex to do the trimming? regex="[" + charToTrim + "]{1,}$"; toBeTrimmed.replace(regex,"");  I don't think there is a .replaceLast() like there is a .replaceFirst()
	public static String trailing(String toBeTrimmed, String charToTrim) {

		String trimmed = toBeTrimmed;

		while ( (trimmed.length() > 0) && ( trimmed.substring(trimmed.length()-1,trimmed.length()).equals(charToTrim) ) ) {
			trimmed = trimmed.substring(0,trimmed.length()-1);
		}

		return trimmed;
	}
}
