package com.williambdavisjr.quotation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.williambdavisjr.util.String.CSVParser;
import com.williambdavisjr.util.String.Trim;


public class QuoteFileReader {

	private static boolean inDebugMode;
	
	public static List<Quotation> readQuotesFromFile(String filespec) {

		List <Quotation> quotes = new ArrayList<Quotation>();
		
		File quoteFile = new File(filespec);
		
		if (!quoteFile.exists()) {
			System.err.println("File " + filespec + " does not exist.");
			return null;
		}
		
		if (filespec.toLowerCase().endsWith(".txt")) 
		{
			quotes = handleTxtFile(quoteFile);
		} 
		if (filespec.toLowerCase().endsWith(".quotes")) 
		{
			quotes = handleTxtFile(quoteFile);
		} 
		else if (filespec.toLowerCase().endsWith(".csv")) 
		{					
			quotes = handleCsvFile(quoteFile);
		}  
		else if (filespec.toLowerCase().endsWith(".xml"))
		{			
			quotes = handleXmlFile(quoteFile);
		}
		else if (filespec.toLowerCase().endsWith(".flat")) 
		{
			quotes = handleFlatFile(quoteFile);
		} 
		else 
		{
			// unknown file type
			System.err.println(quoteFile.getName() + " is an unknown file type; please try a different file.");
			//TODO: throw file not found exception or something?  Treat as text file?
		}			
		
		return quotes;
	}
	

	//TODO: Create an QuoteFileReader class (interface? abstract class?) and make all the handle*File() methods be subclasses. 
	
	public static List<Quotation> handleTxtFile(File quoteFile) {

		String quote;
		String attribution;
		String vocation;
		String context;
		String yearBorn;
		String yearDied;
		int starRating; //default = 0 (unrated)
		String stars;
		String dateOfQuote;
		String originalAttribution; 
		
		long lineCount  = 0L;
		long quoteCount = 0L;

		List <Quotation> quotes = new ArrayList<Quotation>();
		
		try 
		{
			FileReader quoteReader = new FileReader(quoteFile);
			BufferedReader buffer = new BufferedReader(quoteReader);

			while ((quote = buffer.readLine()) != null)
			{
				lineCount++;

				attribution = "";
				vocation = "";
				context = "";
				yearBorn = "";
				yearDied = "";
				stars = "";
				starRating = 0; //default = 0 (unrated)
				dateOfQuote = "";
				originalAttribution = "";

				if (quote.trim().equals("")) 
				{
					// ignore blank lines
					continue;
					
				} else 
				{
					// we have quote text text;
					if (quote.trim().startsWith("-")) 
					{
						// it is an attribution line; if so, it's out of order.
						System.err.println("/!\\ Warning: Skipping an attribution line out of order at quote # " + quoteCount + " line #" + lineCount +": "+ quote);
						continue;
					}

					// clean up contents of quote to avoid problems with embedded quotation marks already in the quote.
					
					quote = Trim.leadingAndTrailing(quote, "\"");			// strip leading/trailing quotes
					quote = quote.replace("\"", "'");		// transform internal double-quotes to single-quotes. Excel hates internal quotes and escapes. Handles very strangely.						
					
					// get next line, assume it's the attribution (who/when quote was said/written)
					attribution = buffer.readLine().trim(); lineCount++;
					
					if (attribution.equals("")) 
					{
//						We found a blank line after the quote line,  so assume no attribution for this quote.
						attribution = "Unknown";	
					} 
					else 
					{
//						We got a non-blank line, assume it is attribution
						
						originalAttribution = attribution;					// keep it pre-modification and extraction
																			//TODO: keep original quote text too, unmodified?
																			//TODO: Make a ConvertedQuote object extending Quote, with those fields, for use in file conversion.

						//TODO: Perhaps we should NOT do some of the following as it may screw up following extractions of years, dates, etc.
						//TODO: Change trimChar to use "^[\\"-]" and "[\\"]$" to remove leading/trailing quotes?
						attribution = Trim.leadingAndTrailing(attribution,"\"");			// remove leading/trailing quotes
						attribution = Trim.leadingAndTrailing(attribution,"�");			// remove curly open-quotes.
						attribution = Trim.leadingAndTrailing(attribution,"�");			// remove curly close-quotes.
						//TODO: How about curly single quotes:  � �
						attribution = attribution.trim();  					// remove any leading whitespace AGAIN after removing surrounding quotes.
						attribution = Trim.leadingAndTrailing(attribution,"-");			// chop off any leading "-" char(s)
						attribution = Trim.leadingAndTrailing(attribution,"�");			// also em-dash (or is it an en-dash?)
						//TODO: add removing of other typographic dash (em or en) in leading position.
						//TODO: need trimLeadingChar() and trimTrailingChar() methods, have trimChar() use both, rename it to trimLeadingAndTrailingChars();
						attribution = attribution.trim(); 					// remove any leading whitespace AGAIN after removing the starting "-".
						attribution = attribution.replace("\"","'");		// transform internal double-quotes to single-quotes. Excel hates internal quotes and escapes. Handles very strangely.

						if (inDebugMode) System.err.println(originalAttribution);
						if (inDebugMode) System.err.println(attribution);
						
//						Objects for regex string searches below.
						Pattern pattern;
						Matcher matcher;
						
						//TODO: Check for "-" or en-dash or em-dash or non-alphanumeric at start that isn't a quote?

						// Find born/died years of the form "(yyyy-yyyy)" (with possible spaces
						String regexYearBornDied = "[, (/]{1}[0-9]{4}[ ]*-[ ]*([0-9]{4})*[), ]{1}";	
						String yearBornDied = "";
						pattern = Pattern.compile(regexYearBornDied);
						matcher = pattern.matcher(attribution); 
						if (matcher.find()) {										
							yearBornDied = matcher.group();
							if (inDebugMode) System.err.println(yearBornDied);
						}						
												
//						 Find year of form "(yyyy-"  from "(yyyy-yyyy)" found above and extract as year born
						String regexYear = "[0-9]{4}";	
						pattern = Pattern.compile(regexYear);
						matcher = pattern.matcher(yearBornDied); 
						if (matcher.find()) {			
							yearBorn = matcher.group();
							if (inDebugMode) System.err.println("Born:"+yearBorn);
						}						
						if (matcher.find()) {
							yearDied = matcher.group();
							if (inDebugMode) System.err.println("Died:"+yearDied);
						}						

						// remove (yyyy-yyyy) from attribution string
						if (!yearBorn.equals("")) {
							if (inDebugMode) System.err.println(attribution);	
							attribution = attribution.replaceFirst(regexYearBornDied,"");
							if (inDebugMode) System.err.println(attribution);	
						}
						
//						Extract stand-alone year of quote of form "9999" surrounded only by parens, spaces or commas (or EOL?) as year of quote using regex
						String regexYearOfQuote = "[, (]{1,}[0-9]{4}[), ]{1,}";
						pattern = Pattern.compile(regexYearOfQuote);
						matcher = pattern.matcher(attribution);
						if (matcher.find()){
							if (inDebugMode) System.err.println("Year of quote found:"+matcher.group());
							Pattern pattern2=Pattern.compile(regexYear);
							Matcher matcher2 = pattern2.matcher(matcher.group());
							if (matcher2.find()) {
								dateOfQuote = matcher2.group();
								if (inDebugMode) System.err.println("Year of quote extracted:"+ dateOfQuote);
								if (inDebugMode) System.err.println(attribution);
								attribution = attribution.replaceFirst(regexYearOfQuote,"");
								if (inDebugMode) System.err.println(attribution);
							}
						}

//						TODO: Extract date of quote e.g.   "Mar[ch] 9, 1999" and "[(][m]m/[d]d/[yy]yy[)]" (what about other forms e.g. dd/mm/yyyy dd mmm, yyyy etc? Add multiple checks as needed!)
//						TODO: Add "date of quote" and perhaps "year of quote" fields too, to supplement the existing yearBorn and yearDied fields.
						String regexMonthDDYear = "[,?!. ]{1,}[A-Z]{1}[a-z]{2,}[ ]+[0-9]{1,2}[, ]+[0-9]{4}";  //   Mon[thname] [0]9, 2011  - what about '11?
						String regexMM_DD_YYYY  = "[,?!. ]{1,}[0-9]{1,2}[-/]{1}[0-9]{1,2}[-/]{1}[0-9]{2,4}";  //  [m]m/[d]d/[yy]yy

						pattern = Pattern.compile(regexMonthDDYear);
						matcher = pattern.matcher(attribution);
						if (matcher.find()){
							dateOfQuote = matcher.group(); //TODO: this overwrites any extracted (yyyy) above; check overwrite before doing it?
							if (inDebugMode) System.err.println("Date of Quote:"+dateOfQuote);
							if (inDebugMode) System.err.println(attribution);
							attribution = attribution.replaceFirst(regexMonthDDYear,"");
							if (inDebugMode) System.err.println(attribution);
						}

						pattern = Pattern.compile(regexMM_DD_YYYY);
						matcher = pattern.matcher(attribution);
						if (matcher.find()){
							dateOfQuote = matcher.group(); //TODO: this overwrites any extracted (yyyy) above; check overwrite before doing it?
							if (inDebugMode) System.err.println("Date of Quote:"+dateOfQuote);
							if (inDebugMode) System.err.println(attribution);
							attribution = attribution.replaceFirst(regexMM_DD_YYYY,"");
							if (inDebugMode) System.err.println(attribution);
						}

						
//						Search for "*", count them and come up with an integer 1-5 as the starRating; No *'s returns 0; 0 means unrated.	
						String regexStars = "[ ,]{1,}[\\*]{1,}"; 
						pattern = Pattern.compile(regexStars);
						matcher = pattern.matcher(attribution);
						if (matcher.find()) {
							stars = attribution.substring( matcher.start()+1, matcher.end() ).trim();
							starRating = stars.length();
								
							if (inDebugMode) System.err.println(attribution);							
							attribution = attribution.replaceFirst(regexStars, "");							
							if (inDebugMode) System.err.println(attribution);
						
						}

//						 Extract 'quoted' strings as context field.  Also (parenthesized strings)
						String regexContext = "['(]{1}[A-Z a-z][)']{1}";
						pattern = Pattern.compile(regexContext);
						matcher = pattern.matcher(attribution);
						if (matcher.find()) {
							context = attribution.substring( matcher.start()+1, matcher.end()-1 );  // or matcher.group()
							if (inDebugMode) System.err.println(context);
							
							if (inDebugMode) System.err.println(attribution);
							attribution = attribution.replaceFirst(regexContext, "");
							if (inDebugMode) System.err.println(attribution);

						}
						
//						Extract NON-quoted/parenthesized strings after a comma or a colon as "vocation".
						String regexVocation = "[:,]{1}[^'(]{1}[A-Z, a-z][^)']{1}$";
						pattern = Pattern.compile(regexVocation);
						matcher = pattern.matcher(attribution);
						if (matcher.find()) {
							vocation = attribution.substring( matcher.start()+1, matcher.end()-1 );  // or matcher.group()
							if (inDebugMode) System.err.println(vocation);
							
							if (inDebugMode) System.err.println(attribution);
							attribution = attribution.replaceFirst(regexVocation, "");
							if (inDebugMode) System.err.println(attribution);

						}
						
					}
					
					if (inDebugMode) {
						System.err.println(quote);
						System.err.println(" " + attribution);
						System.err.println(" " + originalAttribution);
						System.err.println("");
					}

					if (inDebugMode) System.err.println("--------------------------------------------------------------------------------");
					
					quotes.add(new Quotation(quoteCount, quote, attribution, vocation, context, yearBorn, yearDied, starRating, dateOfQuote, originalAttribution));
					quoteCount++;
				}
			}
			buffer.close();
			quoteReader.close();								
		}
		catch (Exception e) 
		{
			System.err.println("Error processing line #" + lineCount + " of quote file '" + quoteFile.getAbsoluteFile()+ "'");
			System.err.println(" Error message: " + e.getMessage());
			e.printStackTrace();
			System.exit(0);
		}
		
		return quotes;
	}
	
	public static List<Quotation> handleCsvFile(File quoteFile) {
		
		String quoteLine = ""; //TODO: Change to StringBuffer or String* class?
		List<Quotation> quotes = new ArrayList<Quotation>();
		
		long lineCount  = 0L;
		long quoteCount = 0L;

		try 
		{
			FileReader quoteReader = new FileReader(quoteFile);
			BufferedReader buffer = new BufferedReader(quoteReader);

			while ((quoteLine = buffer.readLine()) != null )
			{
				lineCount++;
//				TODO add "if commands.contains("debug")" before doing the sytem.out.println below.
//				System.out.println(quote);
				Quotation quotation = parseQuote(quoteLine);
				quotes.add(quotation);
				quoteCount++;
			}
			buffer.close();
			quoteReader.close();
		}
		catch (Exception e) 
		{
			System.err.println("Error processing quote file '" + quoteFile.getAbsoluteFile()+ "'");
			System.err.println(" Error message " + e.getMessage());
			//System.exit(0);
			//TODO: throw file not found or other file i/o exceptions
		}
		
		return quotes;
		
	}

	
	public static List<Quotation> handleXmlFile(File quoteFile) {
//		TODO: Handle XML files created by Flat2XML project in this workspace.
		List<Quotation> quotes = new ArrayList<Quotation>();
		System.err.println("*.xml file format is not yet implemented; please try a different file.");
		return quotes;
	}

	
	public static List<Quotation> handleFlatFile(File quoteFile) {
		// TODO: Add support for flat file from Flat2XML project in this workspace? Copy code from Flat2XML. Make it's own class.
		List<Quotation> quotes = new ArrayList<Quotation>();
		System.err.println("*.flat file format is not yet implemented; please try a different file.");
		return quotes;
	}	

	public static Quotation parseQuote(String quoteToParse)
	{
		String [] quoteArray = {};

		Quotation quotation = null;

		try
		{
			//TODO: this does not deal with commas inside quoted strings.  Change to "\"," ?)
			//quoteArray = quoteToParse.split(",");
			//quoteArray = quoteToParse.split("\"[ ]*,");

			//(?:(?<=")([^"]*)(?="))|(?<=,|^)([^,]*)(?=,|$)
			// http://stackoverflow.com/questions/1441556/parsing-csv-input-with-a-regex-in-java
			
			//quoteArray = quoteToParse.split("(?:(?<=\")([^\"]*)(?=\"))|(?<=,|^)([^,]*)(?=,|$)");

			CSVParser myCSV = new CSVParser();
			quoteArray = myCSV.parse(quoteToParse);
        }
		catch (Exception e)
		{
			System.err.println("Error splitting quote into separate field:" + quoteToParse);
			System.err.println(" Error message:" + e.getMessage());
		}

		if (quoteArray.length != 8)
		{
			System.err.println("Skipping quote line; it may not be formatted properly or have all 8 fields, found " +quoteArray.length + " fields: " + quoteToParse);
			System.err.println("  Fields extracted:");
			for (int i=0; i < quoteArray.length; i++) 
			{
				System.err.println("    "+i+": " + quoteArray[i]);
			}
		}
		else
		{
			for (int i = 0; i < quoteArray.length; i++)
			{
				try
				{
//					TODO add debugging parameter so we can output this if desired.
//					System.out.println(i+": "+quoteArray[i]);
					quoteArray[i] = quoteArray[i].trim();
					quoteArray[i] = Trim.leadingAndTrailing(quoteArray[i],"\"");
//					System.out.println(i+": "+quoteArray[i]);
				}
				catch (Exception e)
				{
					System.err.println("Error parsing quote field #" + (i+1) + " in line: " + quoteToParse);
					System.err.println(" Error message:" + e.getMessage());
				}
			}
			
			try
			{
				  quotation = new Quotation(	Long.parseLong(quoteArray[0]),
												quoteArray[1],
												quoteArray[2],
												quoteArray[3],
												quoteArray[4],
												quoteArray[5],
												quoteArray[6],
												Integer.parseInt(quoteArray[7]),
												"",	// dateOfQuote
												""	// attribution
											);						
			}
			catch (Exception e)
			{
				System.err.println("Error adding quotation to list: " + quoteToParse);
				System.err.println(" Error message: " + e.getMessage());
			}			
		}
		
		return quotation;
	
	}


}
