/* Quotations.sql */

/* Creates the database and populates it with some initial data*/


Drop database if exists `quotations`;
/* Drop schema `quotations`; */

Create database if not exists `quotations`;

Use `quotations`;


/* Create the tables */

Drop table if exists `quotes`;

Create table `quotes` (
  `id` 	 	 int(10) 	NOT null	auto_increment,		/* unique id number for the quote */
  `text` 	 text	 	NOT null 	default '',		/* text of the quote */	/* make this a unique key/field to avoid misattributions? */
  `personId` 	 int(1)	 	NOT null 	default '0',		/* id in `person` table for name of person who said the quote */
  `vocation`	 varchar(25)			default NULL,		/* what the person quoted does for a living */  /* make this a separate table?*/
  `context`	 varchar(100)			default NULL,		/* what context, e.g. book, etc the quote was said in */
  `yearBorn`  	 varchar(4)			default NULL,		/* the year the perseon was born */
  `yearDied` 	 varchar(4)			default NULL,		/* the year the person died, if they have */
  `starRating`   int(1)				default '0';		/* Star rating 1-5, default '0'=unrated?  Or null=unrated? */
  `categoriesId` int(10) 	NOT null 	default '0',
  Primary key		(`id`),
  Key `FKcategories` 	(`categoriesId`),
  Constraint Unique 	(`id`),
  Constraint `FKcategories` Foreign key (`categoriesId`) references `categories` (`quoteId`)
  Constraint `FKperson`     Foreign key (`personId`)     references `persons`    (`id`)
) ;


Drop table if exists `category`;

Create table `category` (
  `id` 		int(10) 	NOT null	auto_increment,
  `name` 	varchar(50)	NOT null,
  Primary key (`id`),
  Constraint Unique (`id`)
  Constraint Unique (`name`)
) ;


Drop table if exists `persons`;

Create table `persons` (
  `id` 		int(10) 	NOT null 	auto_increment,
  `name` 	varchar(50) 	NOT null,
  Primary key (`id`),
  Constraint Unique (`id`)
  Constraint Unique (`name`)
) ;


/* One quote may have many categories */

Drop table if exists `categories`;

Create table `categories` (
  `id` 		int(10) NOT null	auto_increment,  
  `quoteId` 	int(10) NOT null,
  `categoryId`	int(10) NOT null	default '0',
  Primary key 	   	(`quoteId`),
  Key `FKquote`    	(`quoteId`),
  Key `FKcategory` 	(`categoryId`),
  Constraint Unique 	(`id`),	
  Constraint `FKquote`		Foreign key (`quoteId`)    references `quotes`   (`id`)
  Constraint `FKcategory`	Foreign key (`categoryId`) references `category` (`id`)
) ; 



/* Populate the tables with some initial values */

Insert into `persons` values
  (0,'Unknown'),    		/* there are always "unknown" quotes, make that the default person */
  (1,'Joy Frank');  		/* quote #1 linked to this person */

Insert into `category` values
  (0,'None'), 			/* Make "None" the default category */
  (1,'Cats'),			/* The funniest quotes are about cats */
  (2,'Books'),			/* The most intelligent sounding quotes are about books */
  (3,'Computers');		/* Cats, books, computers...life is good! */

Insert into `quotes` values
 (1,'Cats are intended to teach us that not everything in nature has a function.',1,NULL,NULL,1);


Insert into `categories` values
 (1,1,1); 			/* link quote #1 to category 1 */

/* End of Quotations.sql */